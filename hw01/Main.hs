-- Advanced Programming, HW 1
-- by Joshua Margolis, jmarg
-- and <PARTNER NAME>, <PARTNER PENNKEY>

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults  #-}

{-# LANGUAGE NoImplicitPrelude #-}

module Main where
import Prelude hiding (all, reverse, takeWhile, zip)
import Test.HUnit 
import Control.Applicative ((<*>))

main :: IO ()
main = do 
   _ <- runTestTT $ TestList [ test0, test1, test2, test3, test4 ]
   return ()

-- Part 0 (a)


abc :: Bool -> Bool -> Bool -> Bool
abc x y z = x && (y || z)

t0a :: Test
t0a = "0a1" ~: TestList [abc True False True ~?= True, 
                         abc True False False ~?= False,
                         abc False True True ~?= False]

-- 0 (b)

arithmetic :: ((Int, Int), Int) -> ((Int,Int), Int) -> (Int, Int, Int)
arithmetic ((a, b), c) ((d, e), f) = (x, y, z)
  where x = (b*f) - (c*e)
        y = (c*d) - (a*f)
        z = (a*e) - (b*d)

t0b :: Test
t0b = "0b" ~: TestList[ arithmetic ((1,2),3) ((4,5),6) ~?= (-3,6,-3), 
                        arithmetic ((3,2),1) ((4,5),6) ~?= (7,-14,7) ]

-- 0 (c)
cmax :: [Int] -> Int -> Int
cmax l z = g (length l - 1) z
         where g n t
                | n < 0        = t
                | (l !! n) > t = g (n - 1) (l !! n)
                | otherwise    = g (n- 1) t

t0c :: Test
t0c ="0c" ~: TestList[ cmax [1,4,2] 0 ~?= 4, 
                       cmax []      0 ~?= 0,
                       cmax [5,1,5] 0 ~?= 5 ]

-- 0 (d)
reverse :: [a] -> [a]
reverse = foldl (flip (:)) []


t0d :: Test
t0d = "0d" ~: TestList [reverse [3,2,1] ~?= [1,2,3],
                        reverse [1]     ~?= [1] ]

test0 :: Test
test0 = "test0" ~: TestList [ t0a , t0b, t0c, t0d ]


-- Problem 1

bowlingTest0 :: ([ Int ] -> Int) -> Test
bowlingTest0 score = "gutter ball" ~: 0 ~=? score (replicate 20 0)

score0 :: [ Int ] -> Int
score0 _ = 0

bowlingTest1 :: ([ Int ] -> Int) -> Test
bowlingTest1 score = 
   "allOnes" ~: 20 ~=? score (replicate 20 1)

score1 :: [ Int ] -> Int
score1 x = foldr (+) 0 x

bowlingTest2 :: ([ Int ] -> Int) -> Test
bowlingTest2 score = "spareInFirstFrame" ~: 20 ~=? score ([7, 3, 5] ++ (replicate 17 0))

score2 :: [ Int ] -> Int
score2 = score where
   score (x : y : z : xs)
       | x + y == 10 = x + y + z + score2 (z:xs)
       | otherwise = x + y + score2 (z:xs)
   score (x : y : []) = x + y
   score (x : []) = x
   score [] = 0

--TODO: Remake my first score2
score2a :: [ Int ] -> Int
score2a = score where
   score (x : y : z : xs)
     | x + y == 10 = x + y + z + score2 (z:xs)
     | otherwise = x + y + score2 (z:xs)
   score _ = 0

bowlingTest3 :: ([ Int ] -> Int) -> Test
bowlingTest3 score = "strikeInFirstFrame" ~: 19 ~=? score ([10, 5, 4] ++ (replicate 16 0))

score3 :: [ Int ] -> Int
score3 = score where  
   score (10 : x : y : []) = 10 + x + y
   score (10 : x : y : ys) = 10 + x + y + score (x:y:ys)
   score [] = 0
   score _ = 0

bowlingTest4 :: ([ Int ] -> Int) -> Test
bowlingTest4 score = "perfect game" ~: 300 ~=? score (replicate 12 10) 

score4 :: [ Int ] -> Int
score4 = score where
        score (10 : x : y : []) = 10 + x + y
        score (10 : x : y : ys) = 10 + x + y + score (x:y:ys)
        score (x : y : z : zs)
          | x + y == 10 = x + y + z + score2 (z:zs)
          | otherwise = x + y + score2 (z:zs)
        score (x : y : []) = x + y
        score [] = 0
        score _  = 0

test1 :: Test
test1 = TestList (map checkOutput scores) where
  -- the five test cases, in order 
  bowlingTests  = [bowlingTest0, bowlingTest1, bowlingTest2, 
                   bowlingTest3, bowlingTest4]
 
  -- the names of the score functions, the functions themselves, 
  -- and the expected number of passing tests
  scores = zip3 ['0' ..] [score0, score1, score2, score3, score4] [1..]
 
  -- a way to run a unit test without printing output 
  testSilently = performTest (\ _ _ -> return ()) 
                   (\ _ _ _ -> return ()) (\ _ _ _ -> return ()) ()
 
  -- run each bowling test on the given score function, making sure that 
  -- the expected number of tests pass.
  checkOutput (name, score, pass) = " Testing score" ++ [name] ~: do 
    (s0,_) <- testSilently (TestList $ bowlingTests <*> [score])
    assert $ pass @=? cases s0 - (errors s0 + failures s0)

-- Problem 2

-- 2a)

-- | The conv function takes two lists of numbers, reverses the 
-- second list, multiplies their elements together pointwise, and sums
-- the result.  This function assumes that the two input
-- lists are the same length.
 
conv :: [Int] -> [Int] -> Int
conv x y = convZipSum x (reverse y)
    where convZipSum (a:as) (b:bs) = a * b + convZipSum as bs
          convZipSum [] [] = 0

t2a :: Test
t2a = "2a" ~: TestList [conv [2,4,6] [1,2,3] ~?= 20,
                        conv [1] [1]         ~?= 1,
                        conv [2,2] [2,2]     ~?= 8]


-- 2b) 

-- | The normalize function adds extra zeros to the beginning of each
-- list so that they each have length 2n, where n is 
-- the length of the longer number.   
 
normalize :: [Int] -> [Int] -> ([Int], [Int])
normalize x y
    | lenx > leny = ((replicate lenx 0 ++ x), (replicate (2*lenx - leny) 0 ++ y))
    | otherwise = ((replicate (2*leny - lenx) 0 ++ x), (replicate (leny) 0 ++ y))
    where lenx = length x
          leny = length y

t2b :: Test
t2b = "2b" ~: TestList [normalize [1] [2,3] ~?= ([0,0,0,1], [0,0,2,3]),
                        normalize [1] [1]   ~?= ([0,1], [0,1])]

-- 2c)

-- | multiply two numbers, expressed as lists of digits using 
-- the Ūrdhva Tiryagbhyām algorithm.
 --TODO: Implement this
multiply :: [Int] -> [Int] -> [Int]
multiply l1 l2 = 
    let t = normalize l1 l2
        (l1',l2') = (reverse (fst t), reverse (snd t))
    in multiplyAux (l1',l2') [] [] 0 [] where
    multiplyAux :: ([Int],[Int]) -> [Int] -> [Int] -> Int -> [Int] -> [Int]
    multiplyAux (x:xs, y:ys) a b c l = 
        let s = conv (x:a) (y:b)
            z = (s+c) `mod` 10
            c' = (s+c) `div` 10
        in multiplyAux (xs,ys) (x:a) (y:b) c' (z:l)
    multiplyAux _ _ _ _ l = l

t2c :: Test
t2c = "2c" ~: multiply [9,4,6] [9,2,3] ~?= [8,7,3,1,5,8]

-- 2d) (Warning, this one is tricky!)

convAlt :: [Int] -> [Int] -> Int
convAlt a b   = foldc2 (foldl (\x y ->  (*y) : x) [] a) (\x y -> x + y) 0 b
convAlt [] [] = 0

--Fold and applies the partal function as it folds
foldc2 :: [(Int -> Int)] -> (Int -> Int -> Int) -> Int -> [Int] -> Int
foldc2 _ _ z []          = z
foldc2 [] _ z _          = z
foldc2 (f:fs) g z (x:xs) = g (f x) (foldc2 fs g z xs)

--convAlt a b = foldr (\x y -> x + y) foldl (\x y ->  (*y) : x) [] b
--convAlt a b = foldl (\a b -> b ) 0 (foldr (\x y -> x : y) [] b)


--foldl (flip (:)) [] "Hello"
--convAlt [] []     = 0
--convAlt (x:xs) [] = foldl f (f x) xs

--foldr k z xs = go xs
--             where
--               go []     = z
--               go (y:ys) = y `k` go ys
--foldl f z0 xs0 = lgo z0 xs0
--             where
--                lgo z []     =  z
--                lgo z (x:xs) = lgo (f z x) xs

--               go [] _     = 0
               --go _ []     = 0
               --go xs (y:ys) = (y * (xs !! n)) + (go xs ys)
               --    where n = length xs - 1

--convAlt (x : xs) y = x * (last y) + convAlt xs (init y)
--convAlt [] [] = 0
--convAlt _ _   = 0 --Should never reach this case, lengths were different

t2d :: Test
t2d = "2d" ~: TestList [convAlt [2,4,6] [1,2,3] ~?= 20,
                        convAlt [1] [1]        ~?= 1,
                        convAlt [2,2] [2,2]    ~?= 8,
                        convAlt [1,4,2] [1,3,9] ~?= 23]

test2 :: Test
test2 = TestList [t2a,t2b,t2c,t2d]

-- Problem 3

test3 :: Test
test3 = "test3" ~: TestList [t3a, t3b, t3c, t3d, t3e, t3f, t3g, t3h]

-- 3 (a)

-- The intersperse function takes an element and a list 
-- and `intersperses' that element between the elements of the list. 
-- For example,
--    intersperse ',' "abcde" == "a,b,c,d,e"
intersperse :: a -> [a] -> [a]
intersperse x y = (foldr (\a b -> a : x : b) [] $ init y) ++ [(last y)]
 
t3a :: Test
t3a = "3a" ~: TestList [intersperse ',' "abcde" ~?= "a,b,c,d,e",
                        intersperse '.' "Hello" ~?= "H.e.l.l.o"]


-- 3 (b)

-- invert lst returns a list with each pair reversed. 
-- for example:
--   invert [("a",1),("a",2)]     returns [(1,"a"),(2,"a")] 
--   invert ([] :: [(Int,Char)])  returns []

--   note, you need to add a type annotation to test invert with []
--    
invert :: [ (a,b) ] -> [ (b,a) ]
invert = map (\(x,y) -> (y,x))
 
t3b :: Test
t3b = "3b" ~: TestList[ invert [("a",1),("a",2)]  ~?= [(1,"a"),(2,"a")] , 
                        invert ([] :: [(Int,Char)]) ~?= [] ]

-- 3 (c)

-- takeWhile, applied to a predicate p and a list xs, 
-- returns the longest prefix (possibly empty) of xs of elements 
-- that satisfy p:
-- For example, 
--     takeWhile (< 3) [1,2,3,4,1,2,3,4] == [1,2]
--     takeWhile (< 9) [1,2,3] == [1,2,3]
--     takeWhile (< 0) [1,2,3] == []

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile f (x:xs)
    | f x = x : takeWhile f xs
    | otherwise = []
takeWhile _ [] = []
 
t3c :: Test
t3c = "3c" ~: TestList[ takeWhile (< 3) [1,2,3,4,1,2,3,4]  ~?= [1,2],
                        takeWhile (< 9) [1,2,3] ~?= [1,2,3],
                        takeWhile (< 0) [1,2,3] ~?= [] ]

-- 3 (d)

-- find pred lst returns the first element of the list that 
-- satisfies the predicate. Because no element may do so, the 
-- answer is returned in a "Maybe".
-- for example: 
--     find odd [0,2,3,4] returns Just 3

find :: (a -> Bool) -> [a] -> Maybe a
find _ [] = Nothing
find f (x:xs)
    | f x       = Just x
    | otherwise = find f xs
 
t3d :: Test
t3d = "3d" ~: TestList [find (odd) [0,2,3,4]  ~?= (Just 3),
                        find (even) [0,2,3,4] ~?= (Just 0),
                        find (odd) [2, 4, 8]  ~?= (Nothing)] 
 

-- 3 (e)

-- all pred lst returns False if any element of lst 
-- fails to satisfy pred and True otherwise.
-- for example:
--    all odd [1,2,3] returns False

all :: (a -> Bool) -> [a] -> Bool
all _ [] = True
all f (x:xs)
    | f x       = all f xs
    | otherwise = False
 
t3e :: Test
t3e = "3e" ~: TestList [all odd [1,2,3]    ~?= False,
                        all even [0,2,4,6] ~?= True] 
 

-- 3 (f)

-- map2 f xs ys returns the list obtained by applying f to 
-- to each pair of corresponding elements of xs and ys. If 
-- one list is longer than the other, then the extra elements 
-- are ignored.
-- i.e. 
--   map2 f [x1, x2, ..., xn] [y1, y2, ..., yn, yn+1] 
--        returns [f x1 y1, f x2 y2, ..., f xn yn]
map2 :: (a -> b -> c) -> [a] -> [b] -> [c]
map2 _ [] _ = []
map2 _ _ [] = []
map2 f (x:xs) (y:ys) = f x y : map2 f xs ys

t3f :: Test
t3f = "3f" ~: TestList [map2 (+) [1] [1]      ~?= [2],
                        map2 (*) [10,5] [1,3] ~?= [10, 15]] 
-- 3 (g)

-- zip takes two lists and returns a list of corresponding pairs. If
-- one input list is shorter, excess elements of the longer list are
-- discarded.
-- for example:  
--    zip [1,2] [True] returns [(1,True)]
zip :: [ a ] -> [ b ] -> [ (a,b) ]
zip [] _ = []
zip _ [] = []
zip (x:xs) (y:ys) = (x,y) : zip xs ys

t3g :: Test
t3g = "3g" ~: TestList [zip [1,2] [True] ~?= [(1,True)],
                        zip [4,3,2] ["Jim", "Bob"] ~?= [(4,"Jim"), (3, "Bob")] ]

-- 3 (h)  WARNING this one is tricky!

-- The transpose function transposes the rows and columns of its argument. 
-- If the inner lists are not all the same length, then the extra elements
-- are ignored.
-- for example:
--    transpose [[1,2,3],[4,5,6]] returns [[1,4],[2,5],[3,6]]

transpose :: [[ Int ]] -> [[ Int ]]
transpose ([]:_) = []
transpose x
    | all (not . null) x = foldr (\z y -> head z : y) [] x : transpose (map tail x)
    | otherwise        = []

--transpose x
--    | all (not . null) x = (map head x) : transpose (map tail x)
--    | otherwise          = []

t3h :: Test
t3h = "3h" ~: TestList [transpose [[1,2,3],[4,5,6]] ~?= [[1,4],[2,5],[3,6]],
                        transpose [[1, 2, 3], [4, 5, 6], [7, 8]] ~?= [[1,4,7],[2,5,8]],
                        transpose [[1,2],[3,4]] ~?= [[1,3],[2,4]],
                        transpose [[1,2],[3,4],[5,6]] ~?= [[1,3,5],[2,4,6]],
                        transpose [[1,2,3],[4,5,6]] ~?= [[1,4],[2,5],[3,6]],
                        transpose [[1,2,3],[4,5,6],[7,8,9]] ~?= [[1,4,7],[2,5,8],[3,6,9]],
                        transpose [[1]] ~?= [[1]]
                        ]
-- Problem 4

lcs :: String -> String -> String   
lcs = error "unimplemented: lcs" 

test4 :: Test
test4 = "4" ~: TestList [ lcs "Advanced" "Advantaged" ~?= "Advaned" ]

test4a :: Test
test4a = "4a" ~: lcs "abcd" "acbd" ~?= "acd"

test4b :: Test
test4b = "4b" ~: lcs "abcd" "acbd" ~?= "abd"