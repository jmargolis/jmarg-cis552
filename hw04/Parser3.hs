{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults  #-}

-- The basic definition of the parsing monad as developed in lecture.
-- Operations for building sophisticated parsers are in the module
-- ParserCombinators.

module Parser2 (Parser                 
                   --get,
                   --choose,
                   --(<|>),
                   --satisfy,
                   --doParse
                   ) where

newtype Parser [a] b = P (a -> [(b, a)])

--doParse :: Parser a b -> [a] -> [(b, [a])] 
--doParse (P p) s = p s