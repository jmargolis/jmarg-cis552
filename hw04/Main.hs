{-# LANGUAGE FlexibleInstances #-}
module Main where 

-- Advanced Programming, HW 4
-- by <YOUR NAME HERE> <pennkey>, <YOUR PARTNER'S NAME> <pennkey>

import Control.Monad

import Text.PrettyPrint (Doc, (<+>),($$),(<>))
import qualified Text.PrettyPrint as PP

--import ParserTrans
import Parser
import ParserCombinators

import Test.HUnit

import Test.QuickCheck

import Data.Map

type Variable = String


data Value =
    IntVal Int
  | BoolVal Bool
  deriving (Show, Eq)
 
data Expression =
    Var Variable
  | Val Value  
  | Op  Bop Expression Expression
  deriving (Show, Eq)
 
data Bop = 
    Plus     
  | Minus    
  | Times    
  | Divide   
  | Gt        
  | Ge       
  | Lt       
  | Le       
  deriving (Show, Eq)

data Statement =
    Assign Variable Expression          
  | If Expression Statement Statement
  | While Expression Statement       
  | Sequence Statement Statement        
  | Skip
  deriving (Show, Eq)

main :: IO () 
main = do _ <- runTestTT (TestList [ t0, 
                                     t11, t12, t13, 
                                     t2 ])
          return ()

-- Problem 0
---------------------------------------------

wFact :: Statement
wFact = Sequence (Assign "N" (Val (IntVal 2))) (Sequence (Assign "F" (Val (IntVal 1))) (While (Op Gt (Var "N") (Val (IntVal 0))) (Sequence (Assign "X" (Var "N")) (Sequence (Assign "Z" (Var "F")) (Sequence (While (Op Gt (Var "X") (Val (IntVal 1))) (Sequence (Assign "F" (Op Plus (Var "Z") (Var "F"))) (Assign "X" (Op Minus (Var "X") (Val (IntVal 1)))))) (Assign "N" (Op Minus (Var "N") (Val (IntVal 1)))))))))

class PP a where
  pp :: a -> Doc

oneLine :: PP a => a -> IO ()
oneLine = putStrLn . PP.renderStyle (PP.style {PP.mode=PP.OneLineMode}) . pp

indented :: PP a => a -> IO ()
indented = putStrLn . PP.render . pp

instance PP Bop where
  pp Plus   = PP.char '+'
  pp Minus  = PP.char '-'
  pp Times  = PP.char '*'
  pp Divide = PP.char '/'
  pp Gt     = PP.char '>'
  pp Ge     = PP.text ">="
  pp Lt     = PP.char '<'
  pp Le     = PP.text "<="

instance PP Value where
  pp (IntVal x)  = PP.int x
  pp (BoolVal x)
      | x         = PP.text "true"
      | otherwise = PP.text "false"

instance PP Expression where
  pp (Var v) = PP.text v
  pp (Val x) = pp x
  --TODO: Refactor this?
  --pp (Op bop eL eR) = pp eL <+> pp bop <+> pp eR
  --pp (Op bop eL eR) = case (eL, eR) of 
  --            ((Op _ _ _), (Op _ _ _)) -> PP.lparen <> pp eL <> PP.rparen
  --                                        <+> pp bop <+>
  --                                        PP.lparen <> pp eR <> PP.rparen
  --            ((Op _ _ _), _)          -> PP.lparen <> pp eL <> PP.rparen
  --                                        <+> pp bop <+> pp eR
  --            (_, (Op _ _ _))          ->  pp eL <+> pp bop <+>
  --                                        PP.lparen <> pp eL <> PP.rparen
  --            otherwise -> pp eL <+> pp bop <+> pp eR
  pp (Op bop e (Op bop2 e1 e2)) = pp e <+> pp bop <+>
                                   PP.lparen <>
                                          pp e1 <+> pp bop2 <+> pp e2 <>
                                   PP.rparen
  pp (Op bop e1 e2)            = pp e1 <+> pp bop <+> pp e2

instance PP Statement where
  pp (Assign v e) = PP.text v <> PP.text " := " <> pp e
  pp (If exp s1 s2)   = PP.text "if" <+> pp exp <+> PP.text "then"
                        $$ PP.nest 2 (pp s1)
                        $$ PP.text "else" 
                        $$ PP.nest 2 (pp s2)    
                        $$ PP.text "endif"

  pp (While exp s)    = PP.text "while" <+> pp exp <+> PP.text "do"
                        $$ PP.nest 2 (pp s)
                        $$ PP.text "endwhile"

  pp (Sequence s1 s2) = pp s1 <> PP.semi
                        $$ pp s2

  pp Skip             = PP.text "skip"

display :: PP a => a -> String
display = show . pp

-- Simple tests 

oneV,twoV,threeV :: Expression
oneV   = Val (IntVal 1)
twoV   = Val (IntVal 2)
threeV = Val (IntVal 3)

t0 :: Test
t0 = TestList [display oneV ~?= "1",
      display (BoolVal True) ~?= "true",        
      display (Var "X") ~?= "X",
      display (Op Plus oneV twoV) ~?= "1 + 2",
      display (Op Plus oneV (Op Plus twoV threeV)) ~?= "1 + (2 + 3)", 
      display (Op Plus (Op Plus oneV twoV) threeV) ~?= "1 + 2 + 3",
      display (Assign "X" threeV) ~?= "X := 3",
      display Skip ~?= "skip"  ]

--- Your own test cases

{-
t0b :: Test
t0b  = display (If (Val (BoolVal True)) Skip Skip) ~?=
       "if true then skip else skip endif"

t0b' :: Test
t0b' = display (If (Val (BoolVal True)) Skip Skip) ~?=
      "if true then\n  skip\nelse  skip\nendif"
-}

t0b :: Test
t0b = display (If (Val (BoolVal True)) Skip Skip) ~?=
      "if true then\n  skip\nelse\n  skip\nendif"

test0 :: Test
test0 = TestList [ t0, t0b, 
                   display wFact ~?= "N := 2;\nF := 1;\nwhile N > 0 do\n  X := N;\n  Z := F;\n  while X > 1 do\n    F := Z + F;\n    X := X - 1\n  endwhile;\n  N := N - 1\nendwhile",
                   display (Op Times (Op Plus oneV oneV) (Op Plus oneV oneV)) ~?=
                               "1 + 1 * (1 + 1)"
                 ]

-- Problem 1
---------------------------------------------

valueP :: Parser Char Value
valueP = intP <|> boolP

intP :: Parser Char Value
intP = IntVal `fmap` ParserCombinators.int

constP :: String -> a -> Parser Char a
constP s x = string s >> return x

boolP :: Parser Char Value
boolP = BoolVal `fmap` (constP "true" True <|> constP "false" False)

opP :: Parser Char Bop
opP = choice [addOp,
              mulOp,
              constP ">" Gt,
              constP ">=" Ge,
              constP "<" Lt,
              constP "<=" Le]

addOp :: Parser Char Bop
addOp = plus <|> minus 
  where plus   = char '+' >> return (Plus)
        minus  = char '-' >> return (Minus)

mulOp :: Parser Char Bop
mulOp = times <|> divide 
  where times  = char '*' >> return (Times)
        divide = char '/' >> return (Divide)

varP :: Parser Char Variable
varP = many1 upper

wsP :: Parser Char a -> Parser Char a
wsP p = do
  many space
  v <- p
  many space
  return v

parenP :: Parser Char a -> Parser Char a
parenP p = do wsP $ char '('
              x <- wsP $ (parenP p <|> p)
              wsP $ char ')'
              return x

exprP :: Parser Char Expression
exprP = wsP $ parenP chain <|> chain
    where chain  = binExp <|> varVal
          varVal = (Val `fmap` valueP) <|> (Var `fmap` varP)
          binExp = do x <- wsP $ (parenP binExp <|> varVal)
                      o <- wsP opP
                      y <- wsP $ (parenP binExp <|> varVal)
                      return $ Op o x y




t11 :: Test
t11 = TestList ["s0"  ~: succeed (parse exprP "1 "),
                "s1"  ~: succeed (parse exprP "1  + 2"),
                "s2"  ~: succeed (parse exprP "  1  +  1   "),
                "s3"  ~: succeed (parse exprP "1+1  + 1 + 1"),
                "s4"  ~: succeed (parse exprP "  X+ 1  "),
                "s5"  ~: succeed (parse exprP "(1)"),
                "s6"  ~: succeed (parse exprP "  1      + 1  "),
                "s7"  ~: succeed (parse exprP "(X)"),
                "s8"  ~: succeed (parse exprP " (  1  )  "),
                "s9"  ~: succeed (parse exprP " 1 + ( 1 + 1 ) "),
                "s10" ~: succeed (parse exprP " ( 1 + 1 ) "),
                --"s11" ~: succeed (parse exprP " ( ( 1 + 2 ) - 3 ) + ( 1 + 3 ) "),
                --"s12" ~: succeed (parse exprP "((1 + 2) - 3) + (1 + 3)"),
                "s13" ~: succeed (parse exprP "((1 + 1))"),
                "s14" ~: succeed (parse exprP "((1))"),
                "s15" ~: succeed (parse exprP "(((1)))")
               ] where
  succeed (Left _)  = assert False
  succeed (Right _) = assert True

statementP :: Parser Char Statement
statementP = wsP $ choice [assignP, ifP, whileP, seqP, skipP]
  where
    assignP = do
      v <- wsP varP
      wsP $ string ":="
      e <- exprP
      return $ Assign v e

    ifP = do
      wsP $ string "if"
      e <- wsP $ exprP
      wsP $ string "then"
      s1 <- wsP $ statementP
      wsP $ string "else"
      s2 <- wsP $ statementP
      return $ If e s1 s2

    whileP = do
      wsP $ string "while"
      e <- wsP $ exprP
      wsP $ string "do"
      s <- wsP $ statementP
      wsP $ string "endwhile"
      return $ While e s

    seqP = do
      s1 <- wsP $ statementP
      wsP $ string ";"
      s2 <- wsP $ statementP
      return $ Sequence s1 s2

    skipP   = do wsP $ string "skip"; return Skip

t12 :: Test
t12 = TestList ["s1" ~: p "fact.imp",
                "s2" ~: p "test.imp", 
                "s3" ~: p "abs.imp" ,
                "s4" ~: p "times.imp" ] where
  p s = do { y <- parseFromFile statementP s ; succeed y }
  succeed (Left _)  = assert False
  succeed (Right _) = assert True

prop_RT :: Statement -> Bool
prop_RT s = case parse statementP red of
                     Right x -> display x == red
                     Left _ -> False
        where red = display s


--Used for generating arbitrary Variables
arbVar :: Gen String
arbVar = elements $ Prelude.map (\x -> x: []) ['A'..'Z']

--instance Arbitrary Variable where 
--  arbitrary = arbVar

instance Arbitrary Value where 
  arbitrary = oneof [ BoolVal `fmap` arbitrary
                    , IntVal  `fmap` arbitrary ]

instance Arbitrary Bop where
  arbitrary = elements [Plus, Times, Minus, Gt, Ge, Lt, Le]

instance Arbitrary Expression where
  arbitrary = frequency [ (1, liftM  Var arbitrary)
                        , (1, liftM  Val arbitrary)
                        , (5, liftM3 Op  arbitrary arbitrary arbitrary) ]

instance (Ord a, Arbitrary a, Arbitrary b) => Arbitrary (Map a b) where
  arbitrary = fmap Data.Map.fromList arbitrary

instance Arbitrary Statement where
  arbitrary = frequency [ (1, liftM2 Assign arbVar arbitrary)
                        , (2, liftM3 If arbitrary arbitrary arbitrary)
                        , (3, liftM2 While arbitrary arbitrary)
                        , (1, liftM2 Sequence arbitrary arbitrary)
                        , (1, return Skip)]
  shrink    = undefined

testRT :: String -> Assertion
testRT filename = do 
   x <- parseFromFile statementP filename 
   case x of 
     Right ast -> case parse statementP (display ast) of
       Right ast' -> assert (ast == ast')
       Left _ -> assert False
     Left _ -> assert False                             

t13 :: Test
t13 = TestList ["s1" ~: testRT "fact.imp",
                --"s2" ~: testRT "test.imp", 
                "s3" ~: testRT "abs.imp" ,
                "s4" ~: testRT "times.imp" ]

-- Problem 2
---------------------------------------------

data Token = 
     TokVar String       -- variables
   | TokVal Value        -- primitive values
   | TokBop Bop          -- binary operators
   | Keyword String    -- keywords        
      deriving (Eq, Show)

keywords :: [ Parser Char Token ]
keywords = Prelude.map (\x -> constP x (Keyword x)) 
             [ "(", ")", ":=", ";", "if", "then", "else",
             "endif", "while", "do", "endwhile", "skip" ]

type Lexer = Parser Char [Token]

lexer :: Lexer 
lexer = sepBy1
        (liftM TokVal valueP <|>
         liftM TokVar varP   <|>
         liftM TokBop opP    <|>
         choice keywords)
        (many space)

valueTP :: Parser Token Value
valueTP = do
    x <- get2 valHelp
    return x

valHelp :: Token -> Value
valHelp (TokVal x) = x


opTP :: Parser Token Bop
opTP = do 
  x <- get2 opHelp
  return x

opHelp :: Token -> Bop
opHelp (TokBop x) = x



statementTP :: Parser Token Statement
statementTP = undefined

t2 :: Test
t2 = TestList [ parse lexer "X := 3" ~?= 
        Right [TokVar "X", Keyword ":=", TokVal (IntVal 3)] ]


