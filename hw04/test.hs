import Parser
import ParserCombinators

chooseP :: Parser a -> Parser a -> Parser a
p1 `chooseP` p2 = P (\cs -> (doParse p1 cs) ++ (doParse p2 cs))

addOp :: Parser (Int -> Int -> Int)
addOp = plus `chooseP` minus 
  where plus   = char '+' >> return (+)
        minus  = char '-' >> return (-)

mulOp :: Parser (Int -> Int -> Int)
mulOp = times `chooseP` divide 
  where times  = char '*' >> return (*)
        divide = char '/' >> return div

sumE1 :: Parser Int
sumE1 = prodE1 >>= rest 
  where rest x = next x <|> return x
        next x = do o <- addOp
                    y <- prodE1 
                    rest $ x `o` y

prodE1 :: Parser Int
prodE1 = factorE1 >>= rest
  where rest x = next x <|> return x
        next x = do o <- mulOp
                    y <- factorE1 
                    rest $ x `o` y

factorE1 :: Parser Int
factorE1 = parenE <|> int
  where parenE = do char '('
                    n <- sumE1 
                    char ')'
                    return n