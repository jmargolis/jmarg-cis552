{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults -fno-warn-orphans #-} 
{-# LANGUAGE TypeSynonymInstances, FlexibleContexts, NoMonomorphismRestriction, 
FlexibleInstances #-}

module Main where

import WhilePP

import Data.Map (Map)
import qualified Data.Map as Map

import Control.Monad.State
import Control.Monad.Error
import Control.Monad.Writer

import Test.HUnit hiding (State)
--import Test.QuickCheck

type Store    = Map Variable Value

evalE ::  (MonadState Store m, MonadError Value m, MonadWriter String m) =>
          Expression -> m Value
evalE (Var v) = do
    m <- get
    safeLookup v m
evalE (Val v) = return v
evalE (Op Plus x y)   = do 
  v1 <- evalE x
  v2 <- evalE y
  funcAppInt v1 v2 (+)
evalE (Op Minus x y)  = do 
  v1 <- evalE x
  v2 <- evalE y
  funcAppInt v1 v2 (-)
evalE (Op Times x y)  = do
  v1 <- evalE x
  v2 <- evalE y
  funcAppInt v1 v2 (*)
evalE (Op Divide x y) = do 
  v1 <- evalE x
  v2 <- evalE y
  funcAppInt v1 v2 (div)
evalE (Op Gt x y)     = do 
  v1 <- evalE x
  v2 <- evalE y
  funcAppBool v1 v2 (>)
evalE (Op Ge x y)     = do 
  v1 <- evalE x
  v2 <- evalE y
  funcAppBool v1 v2 (>=)
evalE (Op Lt x y)     = do 
  v1 <- evalE x
  v2 <- evalE y
  funcAppBool v1 v2 (<)
evalE (Op Le x y)     = do 
  v1 <- evalE x
  v2 <- evalE y
  funcAppBool v1 v2 (<=)

safeLookup :: (MonadState Store m, MonadError Value m, MonadWriter String m) =>
               Variable -> Map Variable Value -> m Value
safeLookup v m = case Map.lookup v m of
                   Just x -> return x
                   _      -> throwError $ IntVal 0

funcAppInt :: (MonadState Store m, MonadError Value m, MonadWriter String m) =>
               Value -> Value -> (Int -> Int -> Int) -> m Value
funcAppInt (IntVal _) (IntVal 0) _ = throwError $ IntVal 1
funcAppInt (IntVal x) (IntVal y) f   = return $ IntVal $ f x y
funcAppInt _ _ _                     = throwError $ IntVal 2   

funcAppBool :: (MonadState Store m, MonadError Value m, MonadWriter String m) =>
                Value -> Value -> (Int -> Int -> Bool) -> m Value
funcAppBool (IntVal x) (IntVal y) f  = return $ BoolVal $ f x y
funcAppBool _ _ _                    = throwError $ IntVal 2

evalS :: (MonadState Store m, MonadError Value m, MonadWriter String m) => Statement -> m ()
evalS (Assign v e) = do
  m <- get
  x <- evalE e
  put $ Map.insert v x m
evalS (If e s1 s2) = do
  t <- evalE e;
  if t == BoolVal True then evalS s1 else evalS s2
evalS Skip = return ()
evalS (Sequence s1 s2) = do
  evalS s1
  evalS s2
evalS (While e s) = evalS $ If e (Sequence s (While e s)) Skip
evalS (Print s e) = do
  tell (s ++ display e)
evalS (Throw e) = do
  v <- evalE e
  throwError v
evalS (Try s x h) = do
  evalS s `catchError` \e -> evalS $ Sequence (Assign x (Val e)) h 

instance Error Value where
    noMsg = IntVal 2

execute :: Store -> Statement -> (Store, Maybe Value, String)
execute st s = (res,
                case exc of
                 Left x -> Just x
                 Right _ -> Nothing,
                str)
    where ((exc, str), res) = runState (runWriterT (runErrorT (evalS s))) st

raises :: Statement -> Value -> Test
s `raises` v = case (execute Map.empty s) of
    (_, Just v', _) -> v ~?= v'
    _  -> undefined

t1 :: Test
t1 = (Assign "X"  (Var "Y")) `raises` IntVal 0

t2 :: Test
t2 = (Assign "X" (Op Divide (Val (IntVal 1)) (Val (IntVal 0)))) `raises` IntVal 1

--TODO: Fails this test
t3 :: Test       
t3 = TestList [ Assign "X" (Op Plus (Val (IntVal 1)) (Val (BoolVal True))) `raises` IntVal 2,      
                If (Val (IntVal 1)) Skip Skip `raises` IntVal 2,
                While (Val (IntVal 1)) Skip `raises` IntVal 2]

mksequence :: [Statement] -> Statement
mksequence = foldr Sequence Skip

testprog1 :: Statement
testprog1 = mksequence [Assign "X" $ Val $ IntVal 0,
                        Assign "Y" $ Val $ IntVal 1,
                        Print "hello world: " $ Var "X",
                        If (Op Lt (Var "X") (Var "Y")) (Throw (Op Plus (Var "X") (Var "Y")))
                                                       Skip,
                        Assign "Z" $ Val $ IntVal 3]
--TODO: Fails this test
t4 :: Test
t4 = execute Map.empty testprog1 ~?=
  (Map.fromList [("X", IntVal 0), ("Y",  IntVal 1)], Just (IntVal 1), "hello world: 0")

testprog2 :: Statement
testprog2 = mksequence [Assign "X" $ Val $ IntVal 0,
                        Assign "Y" $ Val $ IntVal 1,
                        Try (If (Op Lt (Var "X") (Var "Y"))
                                (mksequence [Assign "A" $ Val $ IntVal 100,
                                             Throw (Op Plus (Var "X") (Var "Y")),
                                             Assign "B" $ Val $ IntVal 200])
                                Skip)
                            "E"
                            (Assign "Z" $ Op Plus (Var "E") (Var "A"))]

t5 :: Test
t5 = execute Map.empty testprog2 ~?=
   ( Map.fromList [("A", IntVal 100), ("E", IntVal 1)
          ,("X", IntVal 0), ("Y", IntVal 1)
          ,("Z", IntVal 101)]
          , Nothing 
   , "")

main :: IO ()
main = do 
   _ <- runTestTT $ TestList [ t1, t2, t3, t4, t5 ]
   return ()

--------------------
-- Part 3
--------------------

{- 

instance Arbitrary Value where 
  arbitrary = oneof [ BoolVal `fmap` arbitrary
                    , IntVal  `fmap` arbitrary ]

instance Arbitrary Bop where
  arbitrary = elements [Plus, Times, Minus, Gt, Ge, Lt, Le]

instance Arbitrary Expression where
  arbitrary = frequency [ (1, liftM  Var arbitrary)
                        , (1, liftM  Val arbitrary)
                        , (5, liftM3 Op  arbitrary arbitrary arbitrary) ]

instance (Ord a, Arbitrary a, Arbitrary b) => Arbitrary (Map a b) where
  arbitrary = fmap Data.Map.fromList arbitrary

instance Arbitrary Statement where
  arbitrary = frequency [ (1, liftM2 Assign arbitrary arbitrary)
                        , (2, liftM3 If arbitrary arbitrary arbitrary)
                        , (3, liftM2 While arbitrary arbitrary)
                        , (1, liftM2 Sequence arbitrary arbitrary)
                        , (1, return Skip)]
  shrink    = undefined



-}
