-- Advanced Programming
-- by <YOUR NAME HERE> <pennid> 
-- and <YOUR PARTNER's NAME> <PARTNER's pennid>

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults -fno-warn-orphans #-} 
{-# LANGUAGE FlexibleInstances #-} 

module Sat where

import Data.Map (Map)  -- use finite map data structure from standard library
import qualified Data.Map as Map

import Data.List
import Test.QuickCheck
import Control.Monad

-- | An expression in CNF, the conjunction of clauses
newtype CNF = CNF [ Clause ] deriving (Eq, Ord, Show)
unCNF :: CNF -> [ Clause ]
unCNF (CNF cs) = cs

-- | A clause -- the disjunction of a number of literals
type Clause = [ Lit ]

-- | A literal, either a positive or negative variable
data Lit = Lit Bool Var deriving (Eq, Ord, Show)

-- | A variable 
data Var = A | B | C | D | E | F | G | H | I | J 
  deriving (Read, Eq, Ord, Show, Enum, Bounded)
  

-- | invert the polarity of a literal
invert :: Lit -> Lit
invert (Lit b x) = Lit (not b) x

exampleFormula :: CNF
exampleFormula = CNF [[Lit True A],[Lit True B,Lit False A]]

exampleAssignment :: Map Var Bool
exampleAssignment = Map.fromList [(A, True), (B, True)]

interpLit :: Map Var Bool -> Lit -> Bool
interpLit m (Lit b k) = maybe b (if b then id else not)  (Map.lookup k m)

interp :: Map Var Bool -> CNF -> Bool
interp m (CNF formula) = all (any (interpLit m)) formula 

prop_interpExample :: Bool
prop_interpExample = interp exampleAssignment exampleFormula

dpll :: CNF -> Maybe (Map Var Bool)
dpll (CNF cnf) = loop initial where
  initial :: Solver
  initial = Solver cnf Map.empty
  loop :: Solver -> Maybe (Map Var Bool)
  loop s@(Solver c m)  =
    if c == [] then Just m  -- All clauses simplified -> correct
    else if [] `elem` c then Nothing  -- Empty clause -> error
        else
            loop (simplify s nextLit) `mplus`
            loop (simplify s $ invert nextLit)
            where _ = pureLitAssign $ unitPropagate s --Is this safe? complied
                  nextLit = head $ head c --This should be safe because of the
                                          --two checks above

data Solver = Solver { clauses :: [Clause] , assignment :: (Map Var Bool) }
  deriving (Eq, Ord, Show)

--Given a literal, simplifies the clauses by removing it
--and then by updating the map
simplify :: Solver-> Lit -> Solver
simplify (Solver c m) l = Solver b $
        case l of
        Lit True v  ->   (Map.insert v True m)
        Lit False v ->   (Map.insert v False m)
        where a = filter (l `notElem`) c
              b = (map (filter (/= (invert l))) a)

unitPropagate :: Solver -> Solver 
unitPropagate s = case findNextUnitClause s of
        Just a  -> unitPropagate $ simplify s a
        Nothing -> s                          


--Finds the next unit clause. Returns a Lit if one exists and returns 
findNextUnitClause :: Solver -> Maybe Lit
findNextUnitClause s@(Solver c _) = aux c
   where aux (x:_) = if length x == 1 then Just (head x)
                     else findNextUnitClause s
         aux []     = Nothing


-- | After unit propagation, solver state should contain no more unit 
-- clauses.
prop_unitPropagate :: Solver -> Bool
prop_unitPropagate s = 
  let cs = clauses (unitPropagate s) in
    all (\c -> length c /= 1) cs


-- | If a propositional variable occurs with only one polarity in the
-- formula, it is called pure. Pure literals can always be assigned in
-- a way that makes all clauses containing them true. Thus, these
-- clauses do not constrain the search anymore and can be deleted. 
pureLitAssign :: Solver -> Solver
pureLitAssign s = case nextPureLiteral s of
                    Just a  -> pureLitAssign $ simplify s a
                    Nothing -> s                           

-- Finds a pure literal and returns it if one exists
nextPureLiteral :: Solver -> Maybe Lit
nextPureLiteral (Solver c _) = findNextPureLit c (nub $ concat c)
                    where findNextPureLit :: [Clause] -> [Lit] -> Maybe Lit
                          findNextPureLit c' (l:ls) = if isPure c' l
                               then Just l
                               else findNextPureLit c ls
                          findNextPureLit _ []     = Nothing
                          isPure :: [Clause] -> Lit -> Bool
                          isPure [] _ = True
                          isPure x z = (invert z) `notElem` (concat x)

-- | After pure literal assignment, the solver state should not contain 
-- any pure literals
prop_pureLitAssign :: Solver -> Bool
prop_pureLitAssign s = 
   let cs = clauses (pureLitAssign s) in 
   all (all (\l -> invert l `elem` concat cs)) cs

prop_dpll :: CNF -> Property
prop_dpll c = 
  case dpll c of 
    Just m  ->  property (interp m c)
    Nothing ->  property True  

  
genList :: Gen a -> Gen [a]
genList gen = frequency [ (1, return [])
                        , (7, liftM2 (:) gen $ genList gen) ]


varList :: [Var]
varList = [A, B, C, D, E, F, G, H, I, J]

rClause :: Gen Clause
rClause = genList rLit where
  rLit :: Gen Lit
  rLit = liftM2 Lit rBool rVar
         
rBool :: Gen Bool
rBool = choose (True, False)
          
rVar :: Gen Var
rVar = elements varList

rTup :: Gen a -> Gen b -> Gen (a, b)
rTup = liftM2 (\x y -> (x,y))

rMap :: Gen (Map Var Bool)
rMap = liftM Map.fromList (genList $ rTup rVar rBool)

instance Arbitrary CNF where
   arbitrary = CNF `fmap` genList rClause

instance Arbitrary Solver where
   arbitrary = liftM2 Solver (genList rClause) rMap


main :: IO ()
main = do 
  quickCheck prop_interpExample
  quickCheck prop_unitPropagate
  quickCheck prop_pureLitAssign
  quickCheck prop_dpll
