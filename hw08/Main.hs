-- Advanced Programming
-- by <YOUR NAME HERE> <pennkey> and <PARTNER's NAME HERE> <partner's pennkey>
 
{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults #-}
{-# LANGUAGE KindSignatures, ScopedTypeVariables, FlexibleInstances #-}

module Main where
import Prelude hiding (zipWith,zipWith3)
import Test.QuickCheck hiding (elements)
import Data.List hiding (insert, delete)
import Control.Monad(liftM)

class Set s where
   empty    :: s a
   member   :: Ord a => a -> s a -> Bool
   insert   :: Ord a => a -> s a -> s a
   elements :: s a -> [a]
   delete   :: Ord a => a -> s a -> s a

instance Set AVL where
   empty    = avlEmpty
   member   = avlMember
   insert   = avlInsert
   elements = avlElements
   delete   = avlDelete

prop_empty :: Bool
prop_empty = null (elements (empty :: AVL Int))

prop_elements :: AVL Int -> Bool
prop_elements x = elements x == nub (sort (elements x)) &&
     all (\y -> member y x) (elements x)

prop_insert1 :: Int -> AVL Int -> Bool
prop_insert1 x t = member x $ insert x t

prop_insert2 :: Int -> AVL Int -> Bool
prop_insert2 x t = all (\y -> member y t') (elements t) where 
   t' = insert x t

prop_delete1 :: AVL Int -> Bool
prop_delete1 t = all (\x -> not (member x (delete x t))) (elements t) 

prop_delete2 :: AVL Int -> Bool
prop_delete2 t = all (\(x,y) -> x == y || (member y (delete x t))) allpairs where
  allpairs = [ (x,y) | x <- elements t, y <- elements t ]

prop_delete3 :: AVL Int -> Int -> Property
prop_delete3 t x = not (x `elem` elements t) ==> (delete x t == t)

set_properties :: Property
set_properties = 
  printTestCase "empty"   prop_empty    .&&.
  printTestCase "elts"    prop_elements .&&. 
  printTestCase "insert1" prop_insert1  .&&.
  printTestCase "insert2" prop_insert2  .&&.
  printTestCase "delete1" prop_delete1  .&&.
  printTestCase "delete2" prop_delete2  .&&.
  printTestCase "delete3" prop_delete3 

data AVL e = E           -- empty tree
           | N           -- non-empty tree
               Int       -- cached height of the tree
               (AVL e)   -- left subtree
               e         -- value
               (AVL e)   -- right subtree
  deriving Show

-- | The tree is a binary search tree
prop_bst :: AVL Int -> Bool
prop_bst = prop_sorted_no_dupes

prop_sorted_no_dupes :: AVL Int -> Bool
prop_sorted_no_dupes t = (nub . sort . elements $ t) == elements t

-- | The height at each node is correctly calculated. 
prop_ht :: AVL Int -> Bool
prop_ht E = True
prop_ht (N h l _ r) = (h == buildH l r) &&
                        prop_ht l && prop_ht r

height :: AVL e -> Int
height E = 0
height (N h _ _ _) = h

-- | The balance factor at each node is between -1 and +1.  
prop_balance :: AVL Int -> Bool
prop_balance E = True
prop_balance t@(N _ l _ r) = (abs $ bf t) < 2 &&
              prop_balance l && prop_balance r

-- | Calculate the balance factor of a node
bf :: AVL e -> Int
bf E = 0
bf (N _ l _ r) = height l - height r

avl_properties :: Property
avl_properties = 
  printTestCase "bst"     prop_bst .&&.
  printTestCase "height"  prop_ht .&&.
  printTestCase "balance" prop_balance

instance (Eq a) => Eq (AVL a) where
  (==) E E = True
  (==) (N h l v r) (N h' l' v' r') = (h == h') == (v == v') &&
                             (l == l') && (r == r')
  (==) _ _ = False

instance (Ord e, Arbitrary e) => Arbitrary (AVL e) where
    arbitrary = liftM (foldr insert E) (arbitrary)

--instance (Ord e, Arbitrary e, Num e) => Arbitrary (AVL e) where
--    arbitrary = liftM (foldr insert E) (arbitrary :: Arbitrary (Positive e))

-- | an empty AVL tree
avlEmpty :: AVL e
avlEmpty = E

-- | list the elements in the tree, in order
avlElements :: AVL e -> [e]
avlElements E = []
avlElements (N _ l v r) = avlElements l ++ v : avlElements r

-- | Determine if an element is contained within the tree
avlMember :: Ord e => e -> AVL e -> Bool
avlMember _ E = False
--avlMember i (N _ l v r) = if i == v then True
--                          else (avlMember i l) || (avlMember i r)
avlMember e (N _ l e' r) = e == e' || avlMember e (if e < e' then l else r)
--Testing Trees

t1 :: AVL Int
t1 = (N 2 (N 1 E 3 E) 5 (N 1 E 7 E))

t2 :: AVL Int
t2 = (N 2 E 5 (N 1 E 7 E))

--b1 :: AVL Int
--b1 = (N 3 E 5 (N 2 (N 1 E 6 E) 7 E)) -- bf b1 = -2

b1 :: AVL Int
b1 = (N 3 (N 2 (N 1 E 6 E) 12 E) 16 E)
b2 :: AVL Int
b2 = (N 3 (N 2 E 12 (N 1 E 14 E)) 16 E)
b3 :: AVL Int
b3 = (N 3 E 16 (N 2 (N 1 E 17 E) 20 E))
b4 :: AVL Int
b4 = (N 3 E 16 (N 2 E 20 (N 1 E 22 E)))

b5 :: AVL Int
b5 = N 5 (N 4 (N 3 (N 2 (N 1 E 1 E) 3 E) 4 E) 5 (N 1 E 6 E)) 10 (N 2 E 13 (N 1 E 17 E))

b6 :: AVL Int
b6 = N 4 (N 3 (N 1 E 4 E) 5 (N 2 E 6 (N 1 E 7 E))) 10 (N 2 E 13 (N 1 E 17 E))
g6 :: AVL Int

g6 = N 4 (N 3 (N 1 E 4 E) 5 (N 2 E 6 (N 1 E 7 E))) 10 (N 2 E 13 (N 1 E 17 E))


{-rebalance :: (Ord e) => AVL e -> AVL e
rebalance E = E
rebalance t
  | bf t > 1 = rotateRight t
  | bf t < -1 = rotateLeft t
  | otherwise = t-}

rebalance :: (Ord e) => AVL e -> AVL e
rebalance E = E
rebalance t@(N _ tl e tr)
  | bf t > 1 && bf tl < 0  = rotLR t
  | bf t > 1               = rotLL t
  | bf t < -1 && bf tr > 0 = rotRL t
  | bf t < -1              = rotRR t
  | otherwise = node tl e tr

rotLR :: (Ord e) => AVL e -> AVL e
rotLR (N _ (N _ a e3 (N _ b e4 c)) e5 d) =
    node (node (a) e3 (b)) e4 (node (c) e5 (d))
rotLR x = error "?"

rotLL :: (Ord e) => AVL e -> AVL e
rotLL (N _ (N _ (N _ a e3 b) e4 c) e5 d) =
    node (node (a) e3 (b)) e4 (node (c) e5 (d))
rotLL x = error "?"

rotRL :: (Ord e) => AVL e -> AVL e
rotRL (N _ a e3 (N _ (N _ b e4 c) e5 d)) = 
    node (node (a) e3 (b)) e4 (node (c) e5 (d))
rotRL x = error "?"

rotRR :: (Ord e) => AVL e -> AVL e
rotRR (N _ a e3 (N _ b e4 (N _ c e5 d))) =
    node (node (a) e3 (b)) e4 (node (c) e5 (d))
rotRR x = error "?"

rotateRight :: AVL e -> AVL e
rotateRight E = E
rotateRight (N _ (N _ a vl b) v c) =
  if height a > height b then node a vl (node b v c)
  else node (node a vl bl) v' (node br v c)
                  where (N _ bl v' br) = b
rotateRight (N _ _ _ _) = error "Unable to rotate"

rotateLeft :: AVL e -> AVL e
rotateLeft E = E
rotateLeft (N _ a v (N _ b vr c)) = 
  if height c > height b then node (node a v b) vr c
  else node (node a v bl) v' (node br vr c)
       where (N _ bl v' br) = b
rotateLeft (N _ _ _ _) = error "Unable to rotate"

node :: AVL a -> a -> AVL a -> AVL a
node l v r = N (buildH l r) l v r

--Builds the height of the node given what is below it
buildH :: AVL e -> AVL e -> Int
buildH l r = 1 + max (height l) (height r)

-- | Insert a new element into a tree, returning a new tree
avlInsert :: (Ord e) => e -> AVL e -> AVL e
avlInsert x E = N 1 E x E
avlInsert x t@(N _ l v r)
    | x == v    = t
    | x < v     = rebalance $ node (insert x l) v r
    | otherwise = rebalance $ node l v (insert x r)

prop_insert_preserves :: Int -> AVL Int -> Bool
prop_insert_preserves x t = prop_bst (insert x t) && prop_ht (insert x t)
                            && prop_balance (insert x t)


buildT :: Ord a => [a] -> AVL a
buildT l = foldr insert E l

--avlDelete x t@(N _ E v E) = if x == v then E else t


avlDelete :: Ord e => e -> AVL e -> AVL e
avlDelete _ E = E
avlDelete x t@(N _ l v r)
  | x > v = rebalance $ node l v (avlDelete x r)
  | x < v = rebalance $ node (avlDelete x l) v r
  | otherwise = case (l, r) of
    (E, E) -> E
    ((N _ _ _ _), E) -> l
    (E, (N _ _ _ _)) -> r
    _ -> rebalance $ node l m (avlDelete m r)
          where m = head . elements $ r

{-   | x == v = case (l, r) of
    (E, E) -> E
    ((N _ _ v' _), E) -> l
    (E, (N _ _ v' _)) -> r
    _ -> rebalance $ node (avlDelete m l) m r
          where m = head . elements $ r
  | x > v = rebalance $ node l v (avlDelete x r)
  | x < v = rebalance $ node (avlDelete x l) v r
  | otherwise = error "bleh"-}


    --_ -> if (height l > height r) then
    --            node (avlDelete mo l) mo r
    --          else
    --            node l le (avlDelete le r)
    --                where mo = last . elements $ l
    --                      le = head . elements $ r

--avlDelete x (N _ l v r)
--  | x > v = rebalance $ node l v (avlDelete x r)
--  | x < v = rebalance $ node (avlDelete x l) v r
--  | otherwise = case (l, r) of
--       (E, E) -> E
--       ((N _ _ _ _), E) -> rebalance $ node (avlDelete mu l) mu E
--                             where mu = last . elements $ l
--       (_, _) -> rebalance $ node l mu (avlDelete mu r)
--                      where mu = head . elements $ r

{-avlDelete x t@(N _ E v E) = if x == v then E else t
avlDelete x t@(N _ l v E) = if x == v then rebalance l else
                               if x < v then
                                rebalance $ node (avlDelete x l) v E
                               else
                                t
avlDelete x t@(N _ E v r) = if x == v then rebalance r else
                              if x < v then
                                rebalance $ node E v (avlDelete x r) 
                              else
                                t
avlDelete x (N _ l v r)
  | x > v = rebalance $ node l v (avlDelete x r)
  | x < v = rebalance $ node (avlDelete x l) v r
  | otherwise = rebalance $ node l mu (avlDelete mu r)
       where mu = head . elements $ r-}

prop_delete_preserves :: AVL Int -> Property
prop_delete_preserves t = (prop_balance t) ==> all (\x -> prop_bst (n x) && prop_ht (n x) && prop_balance (n x)) (elements t)
                    where n y = delete y t

temp :: AVL Int -> [Int]   
temp t = foldr (\x y -> if prop_balance (n x) then y else (x:y)) [] (elements t)
                    where n y = delete y t     

j :: AVL Int
j = N 4 (N 2 (N 1 E (-13) E) (-9) E) (-7) (N 3 (N 2 (N 1 E (-6) E) (-4) E) 2 (N 2 (N 1 E 3 E) 7 (N 1 E 12 E)))

  ------
  --- Testing tools
  -------