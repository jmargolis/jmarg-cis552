 
-- Advanced Programming, HW 2
-- by <YOUR NAME HERE> <pennid> 
-- <YOUR PARTNER's NAME> <PARTNER's pennid>

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults #-}
{-# LANGUAGE NoImplicitPrelude #-} 

module Main where
import Prelude hiding (takeWhile,all)
import Test.HUnit      -- unit test support
--import Graphics.Gloss hiding (play) -- graphics library for problem 1
import GlossStub    -- "stubbed" version of gloss if you have trouble
import XMLTypes        -- support file for problem 4 (provided)
import Play            -- support file for problem 4 (provided)

doTests :: IO ()
doTests = do 
  _ <- runTestTT $ TestList [ test0, test1, test2, test3 ]
  return ()

main :: IO ()
main = do 
       doTests
       -- drawCircles   --- graphics demo, change to drawTree for your HW
       drawSierpinski  
       return ()

-- | a basic tree data structure
data Tree a = Tip | Branch a (Tree a) (Tree a) deriving (Show, Eq)

foldTree :: b -> (a -> b -> b -> b) -> Tree a -> b
foldTree e _ Tip     = e
foldTree e n (Branch a n1 n2) = n a (foldTree e n n1) (foldTree e n n2)

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f = foldTree Tip (\x t1 t2 -> Branch (f x) t1 t2) 

-- 0 (a)

-- invertTree takes a tree of pairs and returns a new tree with each pair reversed. 
-- for example:
--   invertTree (Branch ("a",1) Tip Tip) returns Branch (1,"a") Tip Tip

invertTree :: Tree (a,b) -> Tree (b,a)
invertTree = mapTree (\(x, y) -> (y, x))

t0a :: Test 
t0a = "0a" ~: TestList[ invertTree (Branch ("a",1) Tip Tip) ~?= (Branch (1,"a") Tip Tip)]
 

-- 0 (b)

-- takeWhileTree, applied to a predicate p and a tree t, 
-- returns the largest prefix tree of t  (possibly empty) 
-- where all elements satisfy p. 
-- For example, given the following tree

tree1 :: Tree Int
tree1 = Branch 1 (Branch 2 Tip Tip) (Branch 3 Tip Tip)

--     takeWhileTree (< 3) tree1  returns Branch 1 (Branch 2 Tip Tip) Tip
--     takeWhileTree (< 9) tree1  returns tree1
--     takeWhileTree (< 0) tree1  returns Tip

takeWhileTree :: (a -> Bool) -> Tree a -> Tree a
takeWhileTree f = foldTree Tip (\x y z -> case f x of
                                   True -> Branch x y z
                                   _    -> Tip)


t0b :: Test
t0b = "0b" ~: TestList [ takeWhileTree odd (Branch 1 (Branch 3 Tip Tip) (Branch 5 Tip Tip)) 
                                      ~?= (Branch 1 (Branch 3 Tip Tip) (Branch 5 Tip Tip)),
                         takeWhileTree even (Branch 1 (Branch 3 Tip Tip) (Branch 5 Tip Tip)) 
                                      ~?= Tip,
                         takeWhileTree (< 3) tree1  ~?= Branch 1 (Branch 2 Tip Tip) Tip,
                         takeWhileTree (< 9) tree1  ~?= tree1,
                         takeWhileTree (< 0) tree1  ~?= Tip]
 

-- 0 (c) 
 
-- allTree pred tree returns False if any element of tree 
-- fails to satisfy pred and True otherwise.
-- for example:
--    allTree odd tree1 returns False

allTree :: (a -> Bool) -> Tree a -> Bool
allTree f = foldTree (True) (\x y z-> (f x) && y && z)

t0c :: Test
t0c = "0c" ~: TestList [allTree (< 9) tree1  ~?= True,
                        allTree (< 3) tree1  ~?= False]
 

-- 0 (d)

-- map2Tree f xs ys returns the list obtained by applying f to 
-- to each pair of corresponding elements of xs and ys. If 
-- one branch is longer than the other, then the extra elements 
-- are ignored.
-- for example:
--    map2Tree (+) (Branch 1 Tip (Branch 2 Tip Tip)) (Branch 3 Tip Tip)
--        should return (Branch 4 Tip Tip)

map2Tree :: (a -> b -> c) -> Tree a -> Tree b -> Tree c
map2Tree f = foldTree (const Tip)
                         (\x l r t -> case t of
                                        Tip -> Tip
                                        Branch y l2 r2 -> Branch (f x y) (l l2) (r r2))


t0d :: Test
t0d = "0d" ~: TestList [map2Tree (+) (Branch 1 Tip (Branch 2 Tip Tip))
                        (Branch 3 Tip Tip) ~?= (Branch 4 Tip Tip)]

-- 0 (e) 

-- zipTree takes two trees and returns a tree of corresponding pairs. If
-- one input branch is smaller, excess elements of the longer branch are
-- discarded.
-- for example:  
--    zipTree (Branch 1 (Branch 2 Tip Tip) Tip) (Branch True Tip Tip) returns 
--            (Branch (1,True) Tip Tip)

zipTree :: Tree a -> Tree b -> Tree (a,b)
zipTree = map2Tree (\x y -> (x,y))


t0e :: Test
t0e = "0e" ~: TestList [zipTree (Branch 1 (Branch 2 Tip Tip) Tip) (Branch True Tip Tip) ~?=
                        (Branch (1,True) Tip Tip)]

test0 :: Test
test0 = TestList [ t0a, t0b, t0c, t0d, t0e ]

-- | Display a gloss picture, given the name of the window, size of the 
-- window, position, background color and the picture itself. 
displayInWindow :: String -> (Int, Int) -> (Int, Int) -> Color -> Picture -> IO ()
displayInWindow x y z = display (InWindow x y z)

-- | a picture composed of concentric circles
circles :: Picture
circles = pictures (reverse colorCircles) where
   -- a list of circle pictures with increasing radii
   bwCircles :: [Picture]
   bwCircles = map (\f -> circleSolid (25.0 * f)) [1.0 ..]  
   -- a list of builtin colors
   colors :: [Color]
   colors    = [red, blue, green, cyan, magenta, yellow]
   -- a list of colored circles
   colorCircles :: [Picture]
   colorCircles = zipWith color colors bwCircles

-- | draw the concentric circle picture in a window
-- this variable is an "action" of type IO (). Running the action
-- in the main program will open a window (of size (600,600)) and 
-- display the circles.

drawCircles :: IO ()
drawCircles = displayInWindow "Circles" (600,600) (10,10) black circles

-- | a right triangle at position `x` `y` with side length `size`
triangle :: Float -> Float -> Float -> Picture
triangle x y size = line [(x,y), (x+size, y), (x, y-size), (x,y)]

minSize :: Float
minSize = 8.0

-- | a sierpinski triangle
sierpinski :: Float -> Float -> Float -> [ Picture ]
sierpinski x y size = 
  if size <= minSize
  then [ triangle x y size ] 
  else let size2 = size / 2 
       in sierpinski x y size2 ++ 
          sierpinski x (y-size2) size2 ++
          sierpinski (x+size2) y size2

-- | the action to draw the triangle on the screen
drawSierpinski :: IO ()
drawSierpinski = 
   displayInWindow "Sierpinski" (600,600) (10,10) white sierpinskiPicture where
      sierpinskiPicture = color blue (pictures (sierpinski 0 0 256))

-- 1 (a)
-- Given a ratio for the child sizes, and an initial size, the output should 
-- be a tree data structure storing the size of each branch.

calcSize :: Float -> Float -> Tree Float
calcSize a b
    | b < minSize = Tip
    | otherwise   = Branch b newB newB
    where newB = calcSize a (a*b)

t1a :: Test
t1a = "1a" ~: calcSize 0.5 25 ~=?
         Branch 25.0 (Branch 12.5 Tip Tip) (Branch 12.5 Tip Tip)

-- 1 (b)


--The function fractal delta angle x y sizeTree should return a tree of
--pictures, where the root of the tree starts at position (x,y) and 
--draws a line of the given angle. The direction of each of the child trees
--should be computed by adding and subtracting delta from the parent's angle.
fractal :: Float -> Float -> Float -> Float -> Tree Float -> Tree Picture
fractal _ _ _ _ Tip = Tip
fractal d a x y (Branch v l r) = Branch (Line [(y,x), (nY, nX)]) (lB l) (rB r)
    where
      nY = y + v*cos(a)
      nX = x + v*sin(a)
      lB = fractal d (a+d) nX nY
      rB = fractal d (a-d) nX nY

--fractal delta angle x y sizeTree = foldTree (const Tip) (\x l r y -> case z of
--                                                Branch val l r -> Line [(x, 0.0), (val,0.0)]
--                                                Tip -> )
--map2Tree :: (a -> b -> c) -> Tree a -> Tree b -> Tree c

t1b :: Test
t1b = "1b" ~: fractal (pi/2) 0 0 0 (calcSize 0.5 25) ~=? 
               Branch (Line [(0.0,0.0),(25.0,0.0)]) 
                  (Branch (Line [(25.0,0.0),(25.0,12.5)]) Tip Tip) 
                  (Branch (Line [(25.0,0.0),(25.0,-12.5)]) Tip Tip)

-- 1 (c) 

join :: Tree Picture -> Picture
join = Pictures . foldTree [Blank] (\x y z -> y ++ [x] ++ z)


t1c :: Test
t1c = "1c" ~: join (Branch Blank Tip Tip) ~?= Pictures [Blank, Blank, Blank]

-- | create a fractal tree with some initial parameters. Try changing 
-- some of these values to see how that changes the tree. In particular, 
-- try changing the delta for the angle of the branches (initially pi/6 below).
fractalTree :: Picture
fractalTree = color blue (join (fractal (pi/6) (pi/2) 0 0 sizeTree)) where
   sizeTree = calcSize 0.6 150.0

drawTree :: IO ()
drawTree = displayInWindow "MyWindow" (700,700) (10,10) white fractalTree

test1 :: Test
test1 = TestList [t1a,t1b,t1c]

-- 2 (a)

-- The intersperse function takes an element and a list 
-- and `intersperses' that element between the elements of the list. 
-- For example,
--    intersperse ',' "abcde" == "a,b,c,d,e"

intersperse :: a -> [a] -> [a]
intersperse x y = head y : (foldr (\a b -> x : a : b) [] $ tail y)

t2a :: Test
t2a = "2a" ~: TestList [intersperse ',' "abcde" ~?= "a,b,c,d,e",
                        intersperse '.' "Hello" ~?= "H.e.l.l.o"]


-- 2 (b)

-- invert lst returns a list with each pair reversed. 
-- for example:
--   invert [("a",1),("a",2)] returns [(1,"a"),(2,"a")] 

invert :: [ (a,b) ] -> [ (b,a) ]
invert = map (\(x,y) -> (y,x))

t2b :: Test
t2b = "2b" ~: TestList[ invert [("a",1),("a",2)]  ~?= [(1,"a"),(2,"a")] , 
                        invert ([] :: [(Int,Char)]) ~?= [] ]
 

-- 2 (c)

-- takeWhile, applied to a predicate p and a list xs, 
-- returns the longest prefix (possibly empty) of xs of elements 
-- that satisfy p:
-- For example, 
--     takeWhile (< 3) [1,2,3,4,1,2,3,4] == [1,2]
--     takeWhile (< 9) [1,2,3] == [1,2,3]
--     takeWhile (< 0) [1,2,3] == []

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile f = foldr (\x y -> case (f x) of
                        True -> x : y
                        False -> []) []
 
t2c :: Test
t2c = "2c" ~: TestList[ takeWhile (< 3) [1,2,3,4,1,2,3,4]  ~?= [1,2],
                        takeWhile (< 9) [1,2,3] ~?= [1,2,3],
                        takeWhile (< 0) [1,2,3] ~?= [] ]

-- 2 (d)

-- find pred lst returns the first element of the list that 
-- satisfies the predicate. Because no element may do so, the 
-- answer is returned in a "Maybe".
-- for example: 
--     find odd [0,2,3,4] returns Just 3

find :: (a -> Bool) -> [a] -> Maybe a
find f = foldr (\x y -> case (f x,y) of
                        (True, _)        -> Just x
                        (False, Nothing) -> Nothing
                        (False, _)       -> y ) (Nothing)

t2d :: Test
t2d = "2d" ~: TestList [find odd [0,2,3,4] ~?= (Just 3),
                        find even [2,3,4] ~?= (Just 2),
                        find odd [2,4,6,8] ~?= Nothing]


-- 2 (e)

-- all pred lst returns False if any element of lst 
-- fails to satisfy pred and True otherwise.
-- for example:
--    all odd [1,2,3] returns False

all :: (a -> Bool) -> [a] -> Bool
all f = foldr (\x y -> y && f x) True
 
t2e :: Test
t2e = "2e" ~: TestList [all odd [1,2,3]    ~?= False,
                        all even [0,2,4,6] ~?= True] 
 
test2 :: Test
test2 = TestList [t2a, t2b, t2c, t2d, t2e]

-- 3



formatPlay :: SimpleXML -> SimpleXML
formatPlay (PCDATA str) = PCDATA str
formatPlay (Element name (t:ts)) = 


      --case name of
      -- "PLAY" -> Element "html" [Element "body" (map formatPlay (t:ts))]
      -- "TITLE" -> Element "h1" [t]
      -- "PERSONAE" -> Element "h2" [PCDATA "Dramatis Personae"]
      -- _ -> Element "" []
            --_ -> error name
     --"PLAY" -> Element "html" [(Element "body" (map formatPlay t))]
     --"TITLE" -> Element "h1" (map formatPlay t)
     --"PERSONAE" -> Element "h2" ((PCDATA "Dramatis Personae") : (intersperse (Element "br" []) (map formatPlay t))) -- (intersperse (Element "br" []) (map formatPlay t))
     --"PERSONA" -> Element "" (map formatPlay t)
     --"ACT" -> Element "h2" (map formatPlay t)
     --"SCENE" -> Element "h3" (map formatPlay t)
     --"SPEECH" -> Element "" (map formatPlay t)
     --"SPEAKER" -> Element "b" (map formatPlay t)
     --"LINE" -> Element "br" (map formatPlay t)
     --_ -> error name



firstDiff :: Eq a => [a] -> [a] -> Maybe ([a],[a])
firstDiff [] [] = Nothing
firstDiff (c:cs) (d:ds) 
    | c==d = firstDiff cs ds 
    | otherwise = Just (c:cs, d:ds)
firstDiff cs ds = Just (cs,ds)

-- | Test the two files, character by character to determine whether they
-- match.
testResults :: String -> String -> IO ()
testResults file1 file2 = do 
  f1 <- readFile file1
  f2 <- readFile file2
  case firstDiff f1 f2 of
    Nothing -> return ()
    Just (cs,ds) -> assertFailure msg where
      msg  = "Results differ: '" ++ take 20 cs ++ 
            "' vs '" ++ take 20 ds
 

test3 :: Test
test3 = TestCase $ do 
  writeFile "dream.html" (xml2string (formatPlay play))
  testResults "dream.html" "sample.html"

