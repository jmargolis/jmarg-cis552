-- Advanced Programming, CIS 552
-- your name, your partner's name

{-# OPTIONS -Wall -fno-warn-orphans -fwarn-tabs -fno-warn-type-defaults #-}

module Main where

import FunSyntax 
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Monoid
import Control.Monad 
import Test.HUnit
import Test.QuickCheck

data Cont r a = Cont { runCont :: (a -> r) -> r }

instance Monad (Cont r) where
   return x = Cont (\k -> k x)
   m >>= f  = Cont (\k -> runCont m (\a -> runCont (f a) k))

even_cps :: Int -> Cont r Bool
even_cps x = return (x `mod` 2 == 0)

prop_even :: Int -> Bool
prop_even x = runCont (even_cps x) id == even x

-- a
add_cps :: Int -> Int -> Cont r Int
add_cps x y = return $ x + y

prop_add :: Int -> Int -> Bool
prop_add x y = runCont (add_cps x y) id == x + y

-- b
map_cps :: (a -> Cont r b) -> [a] -> Cont r [b]
map_cps _ [] = return []
map_cps f (x:xs) = do
  a <- f x
  b <- map_cps f xs
  return (a:b)

prop_map :: [Int] -> Bool
prop_map x = runCont (map_cps even_cps x) id == map even x

-- c
filter_cps :: (a -> Cont r Bool) -> [a] -> Cont r [a]
filter_cps _ [] = return []
filter_cps f (x:xs) = do
  a <- f x
  b <- filter_cps f xs
  if a == True then return (x:b)
               else return b

prop_filter :: [Int] -> Bool
prop_filter x = runCont (filter_cps even_cps x) id == filter even x

-- d
foldr_cps :: (a -> b -> Cont r b) -> b -> [a] -> Cont r b
foldr_cps _ i [] = return i
foldr_cps f i (x:xs) = do
  a <- f x i
  b <- foldr_cps f a xs
  return b

prop_foldr :: Int -> [Int] -> Bool
prop_foldr b l = runCont (foldr_cps add_cps b l) id == foldr (+) b l

--Checks all of them with 1000 trials
check_preludeFuns :: IO ()
check_preludeFuns = do
  quickCheckWith (stdArgs {maxSuccess=1000}) prop_add
  quickCheckWith (stdArgs {maxSuccess=1000}) prop_map
  quickCheckWith (stdArgs {maxSuccess=1000}) prop_filter
  quickCheckWith (stdArgs {maxSuccess=1000}) prop_foldr

----------------------
---- Problem 2
----------------------

instance Monoid r => MonadPlus (Cont r) where
   mzero       = Cont (\_ -> mempty)
   mplus c1 c2 = Cont (\k -> runCont c1 k `mappend` runCont c2 k)

amb :: MonadPlus m => [a] -> m a
amb xs = msum (map return xs)

evens :: [Int]
evens = runCont cont (\x -> [x]) where
  cont :: Cont [Int] Int
  cont = do
    x <- amb [1,2,3,4,5,6]
    guard (even x)
    return x

multipleDwelling :: Cont [r] (Int,Int,Int,Int,Int)
multipleDwelling = do
  baker    <- amb [1 .. 5]
  cooper   <- amb [1 .. 5]
  fletcher <- amb [1 .. 5]
  miller   <- amb [1 .. 5] 
  smith    <- amb [1 .. 5]
  guard (distinct [baker, cooper, fletcher, miller, smith])
  guard (baker /= 5)
  guard (cooper /= 1)
  guard (fletcher /= 5)
  guard (fletcher /= 1)
  guard (miller > cooper)
  guard (not (adjacent smith fletcher))
  guard (not (adjacent fletcher cooper))
  return (baker, cooper, fletcher, miller, smith)

distinct :: Eq a => [a] -> Bool
distinct [] = True
distinct (x:xs) = all (x /=) xs && distinct xs

adjacent :: (Num a, Eq a) => a -> a -> Bool
adjacent x y = abs (x - y) == 1

--printAns = print . map show $ runCont multipleDwelling (\(a,b,c,d,e) -> [a,
--                                               b, 
--                                               c,
--                                               d,
--                                               e])
--[length a,
--                                               length b,
--                                               length c,
--                                               length d,
--                                               length e])
 
-- a )  

multipleDwellinga :: Cont [r] (Int,Int,Int,Int,Int)
multipleDwellinga = do
  baker    <- amb [1 .. 5]
  cooper   <- amb [1 .. 5]
  fletcher <- amb [1 .. 5]
  miller   <- amb [1 .. 5] 
  smith    <- amb [1 .. 5]
  guard (distinct [baker, cooper, fletcher, miller, smith])
  guard (baker /= 5)
  guard (cooper /= 1)
  guard (fletcher /= 5)
  guard (fletcher /= 1)
  guard (miller > cooper)
  guard (not (adjacent fletcher cooper))
  return (baker, cooper, fletcher, miller, smith)

-- There are five solutions.

-- b )



-- c )

data Father = MrMoore 
    | ColonelDowning 
    | MrHall 
    | SirBarnacleHood 
    | DrParker 
        deriving (Enum, Eq, Ord, Bounded, Show)

data Daughter = MaryAnn | Gabrielle | Lorna | Rosalind | Melissa 
   deriving (Enum, Eq, Ord, Bounded, Show)

type Yacht = Daughter

{-
(1)Mary Ann Moore's father has a yacht and so has each of his four friends: Colonel Downing, Mr. Hall, Sir Barnacle Hood, and Dr. Parker.
Each of the five also has one daughter and each has named his yacht after a daughter of one of the others.
(2)Sir Barnacle's yacht is the Gabrielle, Mr. Moore owns the Lorna; Mr. Hall the Rosalind.
(3)The Melissa, owned by Colonel Downing, is named after Sir Barnacle's daughter.
(4)Gabrielle's father owns the yacht that is named after Dr. Parker's daughter. Who is Lorna's father? -> Ga

Yacht:
Moore - Lorna (2)
Downing - Melissa (3)
Hall - Rosalind (2)
Barnacle - Gabrielle (2)
Parker - MaryAnn (Inference)

Daugters:
Moore - MaryAnn (1)
Downing - 
Hall - 
Barnacle - Melissa (3)
Parker - (4)[Not Gabrielle]

-}

yachts :: Cont [r] (Map Father Daughter, Map Father Yacht)
yachts = do
  downing <- amb [Gabrielle, Lorna, Rosalind]
  hall <- amb [Gabrielle, Lorna]
  parker <- amb [Lorna, Rosalind]

  daughters <- return $ Map.fromList [ (MrMoore , MaryAnn),
                                 (ColonelDowning , downing),
                                 (MrHall , hall),
                                 (SirBarnacleHood, Melissa),
                                 (DrParker , parker) ]

  boats <- return $ Map.fromList [ (MrMoore        , Lorna),
                                 (ColonelDowning , Melissa),
                                 (MrHall         , Rosalind),
                                 (SirBarnacleHood, Gabrielle),
                                 (DrParker       , MaryAnn) ]
  --mdf <- return $ Map.fromList [ (downing, ColonelDowning),
  --                               (hall   , MrHall),
  --                               (parker , DrParker) ]
  --guard(gFatherBoat == parker)

  return (daughters, boats)

----------------------
---- Problem 3
----------------------

data RegExp = Char Char            -- single literal character
            | Alt RegExp RegExp    -- r1 | r2   (alternation)
            | Seq RegExp RegExp    -- r1 r2     (concatenation)
            | Star RegExp          -- r*        (Kleene star)
            | Empty                -- ε, accepts empty string
            | Void                 -- ∅, always fails 
  deriving (Eq, Show)



acc :: RegExp -> String -> Cont Any String
acc (Char c) (x:xs)  = if c == x then return xs else mzero
acc (Char _) []      = mzero
acc (Alt e e') s = acc e s `mplus` acc e' s
acc (Seq e e') s = acc e s >>= \s' -> acc e' s'
acc (Star e)   s = acc (Alt Empty (Seq e (Star e))) s
acc Empty      s = return s
acc Void       _ = mzero

testp3 :: Test
testp3 = TestList [accept (Char 'a') "a" ~?= True,
                   accept (Char 'a') "" ~?= False,
                   accept (Char 'a') "ba" ~?= False,
                   accept (Alt (Char 'a') (Char 'b')) "a" ~?= True,
                   accept (Alt (Char 'a') (Char 'b')) "b" ~?= True,
                   accept (Seq (Char 'a') (Char 'b')) "ab" ~?= True,
                   accept (Seq (Char 'b') (Char 'a')) "ab" ~?= False,
                   accept (Empty) "" ~?= True,
                   accept (Empty) " " ~?= False,
                   accept (Void) "" ~?= False,
                   accept (Void) "asdf" ~?= False]


----------------------
---- Problem 4
----------------------

accept :: RegExp -> String -> Bool
accept r s = getAny $ runCont (acc r s) (Any . null)

abort :: r -> Cont r a
abort r = Cont (\_ -> r)

data Value =
   IntVal  Int
 | BoolVal Bool
 | FunVal (Value -> Cont Value Value)
 -- new! specialized values for signaling errors``
 | ErrorVal Error

data Error = BooleanOpMismatch Bop Value Value
           | ParseError String
           | UndefinedVariable Variable
           | UnexpectedType Value
           | IllegalFunction

 -- you will need to add other constructors to describe the various runtime 
 -- errors from the interpreter
      
  deriving (Show, Eq)

instance Show Value where
   show (IntVal i)   = show i
   show (BoolVal b)  = show b
   show (FunVal _)   = "<function>"
   show (ErrorVal e) = show e

instance Eq Value where
   IntVal i == IntVal j = i == j
   BoolVal i == BoolVal j = i == j
   FunVal _ == FunVal _ = error "Functions cannot be compared"
   ErrorVal e == ErrorVal f = e == f
   _ == _ = False

evalB :: Bop -> Value -> Value -> Cont Value Value
evalB Plus   (IntVal i1) (IntVal i2) = return $ IntVal  (i1 + i2)
evalB Minus  (IntVal i1) (IntVal i2) = return $ IntVal  (i1 - i2)
evalB Times  (IntVal i1) (IntVal i2) = return $ IntVal  (i1 * i2)
evalB Gt     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 > i2)
evalB Ge     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 >= i2)
evalB Lt     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 < i2)
evalB Le     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 <= i2)
evalB b v1 v2 = abort $ ErrorVal (BooleanOpMismatch b v1 v2)

data Environment = Environment 
   { vars :: Map Variable Value, handlers :: [Handler] }

extend :: Variable -> Value -> Environment -> Environment
extend x v s = s { vars = Map.insert x v (vars s) }

pushHandler :: Handler -> Environment -> Environment
pushHandler h s = s { handlers = h : (handlers s) }

type Handler = Value -> Value


evalK :: Expression -> Environment -> Cont Value Value 
evalK (Var v) (Environment m _) = do
  case Map.lookup v m of
       Just x -> return x
       _      -> abort $ ErrorVal (UndefinedVariable v)
evalK (IntExp v) _ = return $ IntVal v
evalK (BoolExp v) _ = return $ BoolVal v
evalK (Op o e1 e2) env = do
  v1 <- evalK e1 env
  v2 <- evalK e2 env
  evalB o v1 v2
evalK (If e e1 e2) env = do
  v <- evalK e env
  case v of 
       BoolVal b -> evalK (if b then e1 else e2) env
       _         -> abort $ ErrorVal (UnexpectedType (v))
evalK (Fun var e) env =
  return $ FunVal (\v -> evalK e (extend var v env))
evalK (App e1 e2) env = do
  fv <- evalK e1 env
  av <- evalK e2 env
  case fv of
     FunVal f -> f av
     _        -> abort $ ErrorVal IllegalFunction
evalK (LetRec x e1 e2) env = do
  let v = runCont (evalK e1 env) id
      env' = extend x v env
  r <- evalK e2 env'
  return r
evalK (Throw e) env@(Environment _ (h:_)) = do
  v <- evalK e env
  return $ h v
evalK (Throw e) env@(Environment _ []) = do
  v <- evalK e env
  return v
evalK (Try e v e') env = do
  evalK e (pushHandler (\z -> runCont (evalK e' (extend v z env)) id) env)
  --case x of
  --  ErrorVal  -> evalK e' (extend v i (pushHandler (\z -> eval e') env))
  --  _          -> return x

eval :: Expression -> Value
eval e = runCont (evalK e (Environment Map.empty [])) id

re :: String -> Value
re line = 
   case parse line of 
     Just e -> eval e
     Nothing  -> ErrorVal (ParseError line)

testsBasic :: Test
testsBasic = TestList [re "3 + 3" ~?= IntVal 6,
                       re "3 * 3" ~?= IntVal 9,
                       re "3 - 3" ~?= IntVal 0,
                       re "4 > 3" ~?= BoolVal True,
                       re "4 < 3" ~?= BoolVal False]

testIf :: Test
testIf = TestList [eval (If (Op Le (IntExp 5) (IntExp 3)) (IntExp 1) (IntExp 2)) ~?= (IntVal 2),
                   eval (If (Op Ge (IntExp 5) (IntExp 3)) (IntExp 1) (IntExp 2)) ~?= (IntVal 1),
                   eval (If (Op Plus (IntExp 5) (IntExp 3)) (IntExp 1) (IntExp 2)) ~?= ErrorVal (UnexpectedType (IntVal 8))]

testFun :: Test
testFun = TestList []

--re "if (4 > 3) then (X = 4) else (X = 5)"s

tests :: Test
tests = TestList [ re "throw 3" ~?= IntVal 3, 
                   re "try throw 1 catch X with X + 2 endwith" ~?= IntVal 3,
                   re "try 1 + 2 catch X with X + 3 endwith" ~?= IntVal 3,
                   re "try (try throw 1 catch X with throw X + 1 endwith) catch Y with Y + 1 endwith" ~?= IntVal 3,
                   re "try (try throw 1 catch X with X + 2 endwith) catch Y with Y + 4 endwith" ~?= IntVal 3]

repl :: IO ()
repl = do
   putStr "%> "
   line <- getLine
   putStrLn $ show (re line)
   repl

