-- Advanced Programming, HW 3
-- by <YOUR NAME HERE> <pennid> 
-- (and  <YOUR PARTNERS NAME> <pennid>)

{-# OPTIONS -Wall -fno-warn-unused-binds -fno-warn-unused-matches -fwarn-tabs -fno-warn-type-defaults #-}
{-# LANGUAGE NoImplicitPrelude, FlexibleInstances #-}

module Main where

import Prelude hiding (mapM,any,all,filter)

import Data.Map (Map)
import qualified Data.Map as Map
import Control.Monad.State hiding (when, mapM, foldM, (>=>), sequence, join)
import Test.HUnit hiding (State)

import Data.List.Split

main :: IO ()
main = return ()

-- Problem 0

maybeList, maybeList2 :: [Maybe Int]
maybeList = [(Just 1), (Just 2), (Just 3), (Just 4), (Just 5)]
maybeList2 = [(Just 1), (Just 2), (Just 3), (Just 4), (Just 5), Nothing]

-- (a) Define a monadic generalization of map 

mapM :: Monad m => (a -> m b) -> [a] -> m [b]
mapM f = foldr d (return [])
       where d a b = do {x <- (f a); xs <- b; return (x:xs)}
 
t0a :: Test 
t0a = "0a" ~: TestList[mapM (\x -> Just x) [1,2,3,4,5]  ~?= Just [1,2,3,4,5]]

-- (b) Define a monadic generalization of foldl

foldM :: Monad m => (a -> b -> m a) -> a -> [b] -> m a
foldM _ z []     = return z
foldM f z (x:xs) = do {a <- f z x; foldM f a xs}
--foldM f z (x:xs) = (f z x) >>= (\a -> foldM f a xs)

t0b :: Test 
t0b = "0b" ~: TestList[foldM (\x y -> Just (x+y)) 6 [1,2,3,4,5]  ~?= Just 21]
                      --Main.foldM (\x y -> return (y : x)) [] ("Hello") ~?= foldl (\x y -> y : x) [] ("Hello")]
  --TODO: Why isn't this test working?
-- (c) 

-- | Evaluate each action in the sequence from left to right,
-- and collect the results.

sequence :: Monad m => [m a] -> m [a] 
sequence = foldr d (return [])
       where d a b = do {x <- a; xs <- b; return (x:xs)}

t0c :: Test 
t0c = "0c" ~: TestList[Main.sequence maybeList  ~?= Just [1,2,3,4,5]]

-- (d)

-- | The fish operator, a form of composition
(>=>) :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
(>=>) f g = \a -> f a >>= g

--TODO: Create a test case for this
t0d :: Test 
t0d = "0c" ~: TestList[Main.sequence maybeList  ~?= Just [1,2,3,4,5]]

-- (e)  
-- | The 'join' function removes one level of monadic structure, 
-- projecting its bound argument into the outer level.
join :: (Monad m) => m (m a) -> m a
join x = x >>= id
-- TODO: Theres got to be a better way
-- join z = do {y <- z; x <- y; return x}

-- Problem 1

data AList a = Nil | Single a | Append (AList a) (AList a) deriving (Show, Eq)

-- (a)


instance Functor AList where
   --fmap :: (a -> b) -> AList a -> Alist b 
   fmap _ Nil          = Nil
   fmap f (Single x)   = Single $ f x
   fmap f (Append l r) = Append (fmap f l) (fmap f r)

instance Monad AList where
   --a -> m a
   return x = Single x
 
  -- m a -> (a -> m b) -> m b
  -- Alist a -> (a -> Alist b) -> Alist b
   Nil        >>= _ = Nil
   Single x   >>= f = f x
   Append l r >>= f = Append (l >>= f) (r >>= f)


prop_FMapId :: (Eq (f a), Functor f) => f a -> Bool
prop_FMapId x = fmap id x == id x

prop_FMapComp :: (Eq (f c), Functor f) => (b -> c) -> (a -> b) -> f a -> Bool
prop_FMapComp f g x = 
   fmap (f . g) x == (fmap f . fmap g) x

prop_LeftUnit :: (Eq (m b), Monad m) => a -> (a -> m b) -> Bool
prop_LeftUnit x f = 
   (return x >>= f) == f x

prop_RightUnit :: (Eq (m b), Monad m) => m b -> Bool
prop_RightUnit m = 
   (m >>= return) == m

prop_Assoc :: (Eq (m c), Monad m) =>
    m a -> (a -> m b) -> (b -> m c) -> Bool
prop_Assoc m f g = 
   ((m >>= f) >>= g) == (m >>= \x -> f x >>= g)

prop_FunctorMonad :: (Eq (m b), Functor m, Monad m) => m a -> (a -> b) -> Bool
prop_FunctorMonad x f = (fmap f x) == (x >>= return . f)

testList :: AList Int
testList = Append (Append (Single 0) (Single 1)) (Single 2)

testAListFunctor :: Test
testAListFunctor = TestList [
    prop_FMapId (Single 1) ~? "True",
    prop_FMapId (testList) ~? "True",
    prop_FMapComp (+1) (*2) (Single 1) ~? "True",
    prop_FMapComp (+1) (*2) (Single 1)  ~? "True"
 ]



testAListMonad :: Test
testAListMonad = TestList [
    prop_LeftUnit (1) (\x -> (Single True)) ~? "True",
    prop_RightUnit (Single True) ~? "True",
    prop_Assoc (Single True) (\x -> Single $ (x && False)) (\y -> Single (y && True)) ~? "True"
  ]


testAListFunctorMonad :: Test
testAListFunctorMonad = TestList [
  prop_FunctorMonad (Single 1) (\x -> [x]) ~? "True",
  prop_FunctorMonad (Single 1) (\_ -> Just "Whatever") ~? "True"]

--TODO: Are you asking us to rewrite these such that they break the rules?
-- (b)

{- Invalid instance of Functor and Monad:
 
instance Functor AList where
   --fmap :: (a -> b) -> AList a -> Alist b 
   fmap f s = undefined

instance Monad AList where
   --a -> m a
   return = undefined

   -- m a -> (a -> m b) -> m b
   -- Alist a -> (a -> Alist b) -> Alist b
   (>>=)  = undefined
 
 -}



-- (c)

-- | access the first element of the list, if there is one.
first :: AList a -> Maybe a 
first Nil = Nothing
first (Single x) = Just x
first (Append l r) = mplus (first l) (first r)

t1c1:: Test 
t1c1 = "1c1" ~: TestList[first seq1 ~=? Just 3,
                       first testList ~=? Just 0]

-- | access the last element of the list, if there is one
final :: AList a -> Maybe a
final (Append l r) = mplus (final r) (final l) 
final x = first x

t1c2 :: Test 
t1c2 = "1c2" ~: TestList[final seq1 ~=? Just 5,
                       final testList ~=? Just 2]
-- (d)

instance MonadPlus AList where
  mzero = Nil
  mplus = Append

search :: MonadPlus m => (a -> Bool) -> AList a -> m a
search f (Nil) = mzero
search f (Single v)
    | f v = return v
    | otherwise = mzero
search f (Append r l) = search f r `mplus` search f l


any :: (a -> Bool) -> AList a -> Maybe a 
any = search

all :: (a -> Bool) -> AList a -> [a]
all = search

filter :: (a -> Bool) -> AList a -> AList a
filter = search

testSearch :: Test
testSearch = TestList [ "search1" ~: any (>3)    seq1 ~=? Just 4, 
                        "search2" ~: all (>3)    seq1 ~=? [4,5],
                        "search3" ~: filter (>3) seq1 ~=? 
                         Append (Append Nil Nil) 
                           (Append (Single 4) (Single 5))] 

seq1 :: AList Int
seq1 = Append (Append Nil (Single 3)) (Append (Single 4) (Single 5))

seq2 :: AList Int
seq2 = Append (Single 4) (Single 5)
-- (e) 

toList :: AList a -> [a]
toList Nil = []
toList (Single x)     = [x]
toList (Append l r) = (aux2 l) . (aux2 r) $ []
--toList (Append Nil Nil) = []
--toList (Append Nil (Single y)) = y : []
--toList (Append (Single x) Nil) = x : []
--toList (Append (Single x) (Single y)) = x : y : []


--toList (Append x y) = (aux2 x)
--toList (Append (Single x) Nil) = x : []
aux2 :: AList a -> ([a] -> [a])
aux2 Nil = id
aux2 (Single x) = (x :)
aux2 (Append l r) = (aux2 l) . (aux2 r)

--toList (Append s1 s2) = toList s1 ++ toList s2

--toList (Append s1 s2) = [x : y :[] | x <- toList s1 , y <- toList s2]
    --aux :: AList a -> AList b -> [a]
    --aux [x] y = x : y
    --aux x [y] = x : y

-- toList (Append s1 s2) = 
-- | length l == 1 = (head l) : 
--    where l = toList s1
--          r = toList s2
--do {x <- toList s1; x : (toList s2)}
 

showAsList :: Show a => AList a -> String
showAsList = show . toList

exAList :: AList Int
exAList = Append (Append (Append (Append (Single 0) 
                                        (Single 1)) 
                                 (Single 2))
                        (Single 3)) 
                 (Single 4)

testToList :: Test
testToList = toList exAList ~?= [0,1,2,3,4]

-- Problem 2

data RegExp = Char Char            -- single literal character
            | Alt RegExp RegExp    -- r1 | r2   (alternation)
            | Seq RegExp RegExp    -- r1 r2     (concatenation)
            | Star RegExp          -- r*        (Kleene star)
            | Empty                -- ε, accepts empty string
            | Void                 -- ∅, always fails 
            | Mark RegExp          -- (for marked subexpressions, see (b) below)
  deriving Show

lower, upper, digit, punc, white, anyc :: RegExp
lower = foldr1 Alt (map Char ['a' .. 'z'])
upper = foldr1 Alt (map Char ['A' .. 'Z'])
digit = foldr1 Alt (map Char ['0' .. '9'])
punc  = foldr1 Alt (map Char "<>!/.*()?@")
white = foldr1 Alt (map Char " \n\r\t")

anyc  = lower `Alt` upper `Alt` digit `Alt` punc `Alt` white

word  :: String -> RegExp
word w = foldr Seq Empty (map Char w)

cis552 :: RegExp
cis552 = word "cis552"

boldHtml :: RegExp
boldHtml = word "<b>" `Seq` Star anyc `Seq`  word "</b>"

plus :: RegExp -> RegExp
plus pat = pat `Seq` Star pat

-- (a)

accept :: RegExp -> String -> Bool
accept (Mark r)    s = accept r s

accept (Char x) [] = False
accept (Char x)    (s:[])
     | x == s = True
     | otherwise = False
accept (Char _) (s:ss) = False

accept (Alt x y)     s = accept x s || accept y s
accept (Seq x y)     s = accept x s && accept y s

accept (Star x)      s = undefined
accept (Star x)     [] = True

accept Empty        "" = True
accept Empty        _  = False

accept Void         _  = False 


-- all decompositions of a string into two different pieces
--     split "abc" == [("","abc"),("a","bc"),("ab","c"),("abc","")]
split :: [a] -> [([a], [a])]
split l = [splitAt v l | v <- [0..length l] ]

--split l = do {x <- l; y <- v; return (splitAt y x) } >>= id
--    where n = length l
--          v = [1..n]

  --foldr (\x y z -> [([],[])]) ([(,)]) (replicate n l) [1..n]
  --       where n = length l
--split = foldr aux []
--    where aux :: a -> b -> b
--          aux x ((i, j):_) =  
--          aux x [] = (x, [])

testSplit :: Test
testSplit = split "abc" ~?= [("","abc"),("a","bc"),("ab","c"),("abc","")]

parts :: [a] -> [[[a]]]
parts l = [splitPlaces p l | p <- []

--parts :: [a] -> [[[a]]]
--parts l = [(\(x,y) -> [x,y]) $ splitAt v l | v <- [1..length l - 1]]


-- all decompositions of a string into multi-part (nonempty) pieces
-- parts "abc" = [["abc"],["a","bc"], ["ab","c"], ["a","b","c"]]

--rem :: [[String]] -> String
--rem = map (\x -> [ y | y <- x])

           
--parts :: String -> [[String]]
--parts str = 
--    [ z | z <- partsAux (powerset str) (length str - 1), concat z == str ]
--    where
--      partsAux :: [String] -> Int -> [[String]]
--      partsAux s 0 = [ [x] | x <- s ]
--      partsAux s n = [ x : xs | x <- s, xs <- partsAux s (n-1)]


--powerset :: String -> [String]
--powerset = foldr (\x acc -> acc ++ map (x:) acc) [[]]

--ps2 :: [a] -> [[a]]
--ps2 = foldr (\x acc -> acc ++ map (x:) acc) [[]]

--parts2

--parts :: [a] -> [[[a]]]
--parts = undefined

--p :: [a] -> [[a]] -> [[a]]
--p a b = a : a 

--sp2 x = foldr p [] $ sp x

--sp :: [a] -> [[[a]]]
--sp l = [(\(x,y) -> [x,y]) $ splitAt v l | v <- [1..length l - 1]]



--partz :: [a] -> [[[a]]]
--partz l = foldr (aux) [] [splitAt v l | v <- [1..(length l - 1)]]

--aux :: ([a], [a]) -> [[[a]]] -> [[[a]]]
--aux (x, y) o
--        | length x > 1 = [x,y] : [(concat . partz $ x)] ++ o
--        | otherwise    = [x,y] : o

--part1 :: [a] -> [[[a]]]
--part1 l = do
             
--aux (x,y) o = [x,y] ++ (aux ([splitAt v x | v <- [0..(length x)]]) o)
        -- | length x > 1 = [x,y] : [(concat $ partz x)] ++ [y] : o
        -- | length y < 1 = [x,y] : [(concat $ partz y)]  ++ o
        -- | otherwise    = [x,y] : o


{-parts l = l : s : concatMap (parts) s
        where s = map . unbox $ [splitAt v l | v <- [1..(length l -1)]]

unbox :: ([a],[a]) -> [[a]]
unbox (x,y) = [x,y]

parts2 l = [ helper (x,y) | (x,y) <- s]
       where s = [splitAt v l | v <- [0..(length l - 1)]]

helper :: ([a],[a]) -> [[a]]
helper (x, y)
        | length x > 1 = [x,y] 
        | otherwise = [x,y]-}

--unbox :: [([a],[a])] -> [[[a]]]
--unbox [] = []
--unbox (([],y):xs) = [y] : unbox xs
--unbox ((x,y):xs)
--        | length x > 1 = [x,y] : (concat . unbox $ parts2 x)  : unbox xs
--        | otherwise = [x,y] : unbox xs
--parts s = concatMap (aux) $ split s
--        where aux i
--               | length i > 1 = split i
--               | otherwise    = [i]

--testParts :: Test
--testParts = TestList [ parts "abc" ~?= [["abc"],["a","bc"], ["ab","c"], ["a","b","c"]],
--                       parts "abcd" ~?=

testAccept :: Test
testAccept = TestList [ 
   not (accept Void "a") ~? "nothing is void",
   not (accept Void "") ~? "really, nothing is void",
   accept Empty "" ~? "accept Empty true", 
   not (accept Empty "a") ~? "not accept Empty",
   accept lower "a" ~? "accept lower",
   not (accept lower "A") ~? "not accept lower",
   accept boldHtml "<b>cis552</b>!</b>" ~? "cis552!",
   not (accept boldHtml "<b>cis552</b>!</b") ~? "no trailing" ]

testAccept2 :: Test
testAccept2 = TestList [
    accept (Star (Char 'a')) "" ~?= True,
    accept (Star Empty) "" ~?= True,
    accept (Star (Char 'a')) "aaaaaa" ~?= True]

-- (b)

-- | Mark a subexpression
-- this function just wraps the data constructor now for future 
-- extensibility, see (d) below
mark :: RegExp -> RegExp  
mark r = Mark r

boldHtmlPat :: RegExp
boldHtmlPat = word "<b>" `Seq` mark (Star anyc) `Seq` word "</b>"

namePat :: RegExp
namePat = mark (plus letter) `Seq` Star white `Seq` mark (plus letter) 
          where letter = Alt lower upper

wordsPat :: RegExp
wordsPat = Star (mark (plus lower) `Seq` Star white)

testPat :: Test
testPat = TestList [
    patAccept boldHtmlPat "<b>cis552" ~?= Nothing,
    patAccept boldHtmlPat "<b>cis552!</b>" ~?= Just ["cis552!"],
    patAccept boldHtmlPat "<b>cis552</b>!</b>" ~?= Just ["cis552</b>!"],
    patAccept namePat "Haskell  Curry" ~?= Just ["Haskell", "Curry"], 
    patAccept wordsPat "a    b c   d e" ~?= Just ["a", "b", "c", "d", "e"]
  ]

patAccept :: RegExp -> String -> Maybe [String]
patAccept = error "patAccept: unimplemented"



-- (c)

match :: RegExp -> String -> Bool
match r s = nullable (foldl deriv r s)

-- | `nullable r` return `True` when `r` matches the empty string
nullable :: RegExp -> Bool
nullable _ = error "nullable: unimplemented"

-- |  Takes a regular expression `r` and a character `c`,
-- and computes a new regular expression that accepts word `w` if `cw` is
-- accepted by `r`.
deriv :: RegExp -> Char -> RegExp
deriv = error "deriv: unimplemented"


-- (d)  OPTIONAL CHALLENGE

-- Problem 3

type Variable = String

data Statement =
    Assign Variable Expression          -- x = e
  | If Expression Statement Statement   -- if (e) {s1} else {s2}
  | While Expression Statement          -- while (e) {s}
  | Sequence Statement Statement        -- s1; s2
  | Skip                                -- no-op
  deriving (Eq, Show)

data Expression =
    Var Variable                        -- x
  | Val Value                           -- v 
  | Op  Bop Expression Expression
  deriving (Eq, Show)

data Bop = 
    Plus     -- +  :: Int  -> Int  -> Int
  | Minus    -- -  :: Int  -> Int  -> Int
  | Times    -- *  :: Int  -> Int  -> Int
  | Divide   -- /  :: Int  -> Int  -> Int
  | Gt       -- >  :: Int -> Int -> Bool 
  | Ge       -- >= :: Int -> Int -> Bool
  | Lt       -- <  :: Int -> Int -> Bool
  | Le       -- <= :: Int -> Int -> Bool
  deriving (Eq, Show)

data Value =
    IntVal Int
  | BoolVal Bool
  deriving (Eq, Show)

type Store = Map Variable Value

evalE :: Expression -> State Store Value
evalE (Var v)    = do {m <- get; return (safeLookup v m)}
evalE (Val v)    = return v
evalE (Op Plus x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppInt v1 v2 (+)) }
evalE (Op Minus x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppInt v1 v2 (-)) }
evalE (Op Times x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppInt v1 v2 (*)) }
evalE (Op Divide x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppInt v1 v2 (div)) }
evalE (Op Gt x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppBool v1 v2 (>)) }
evalE (Op Ge x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppBool v1 v2 (>=)) }
evalE (Op Lt x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppBool v1 v2 (<)) }
evalE (Op Le x y) = do {v1 <- evalE x; v2 <- evalE y; return (funcAppBool v1 v2 (<=)) }

safeLookup :: Variable -> Map Variable Value -> Value
safeLookup v m = case Map.lookup v m of
                      Just x -> x
                      _      -> IntVal 0

funcAppInt :: Value -> Value -> (Int -> Int -> Int) -> Value
funcAppInt (IntVal x) (IntVal y) f  = IntVal $ f x y
funcAppInt _ _ _                    = IntVal 0

funcAppBool :: Value -> Value -> (Int -> Int -> Bool) -> Value
funcAppBool (IntVal x) (IntVal y) f  = BoolVal $ f x y
funcAppBool _ _ _                    = IntVal 0

add1and2 :: Expression
add1and2 = (Op Plus (Val (IntVal 1)) (Val (IntVal 2)))

evalS :: Statement -> State Store ()

evalS w@(While e s)      = do {t <- evalE e; if t == BoolVal True then evalS (Sequence s w) else evalS Skip;}
--                             t <- evalE e;
--                             if (t == BoolVal True)
--                             then do
--                                  y <- evalS s
--                                  x <- evalS (While e s)
--                                  return x
--                             else put
----m <- get;
--                             --x <- if evalS (If e s Skip) == m then put m else return x;
--                             --val <- (evalE e);
--                             y <- evalS s;
--                             --z <- if (val == BoolVal True) then (evalS (While e s)) else put m;
--                            return y}
evalS Skip             = do {m <- get; put m}
evalS (Sequence s1 s2) = do {evalS s1; evalS s2;} -- put m; 
--evalS (Sequence s1 s2) = do {x <- evalS s1; y <- evalS s2;return x; return y;} -- put m; 
evalS (Assign v e)     = do {m <- get; x <- evalE e; put $ Map.insert v x m}
evalS (If e s1 s2)     = do {t <- evalE e; if t == BoolVal True then evalS s1 else evalS s2}

{-    Assign Variable Expression          -- x = e
  | If Expression Statement Statement   -- if (e) {s1} else {s2}
  | While Expression Statement          -- while (e) {s}
  | Sequence Statement Statement        -- s1; s2
  | Skip                                -- no-op-}

execS :: Statement -> Store -> Store
execS s1 m = execState (evalS s1) m

--execState :: State s a -> s -> s
--type Store = Map Variable Value


run :: Statement -> IO ()
run stmt = do putStrLn "Output Store:" 
              print (execS stmt Map.empty)

vX :: Statement
vX = Assign "X" (Val (IntVal 1))

vY :: Statement
vY = Assign "Y" (Val (IntVal 2))

------- Regular ops

plusTest :: Statement
plusTest = Assign "Z" (Op Plus (Val (IntVal 3)) (Val (IntVal 2)))

minusTest :: Statement
minusTest = Assign "X" (Op Minus (Val (IntVal 3)) (Val (IntVal 2)))

timesTest :: Statement
timesTest = Assign "Z" (Op Times (Val (IntVal 3)) (Val (IntVal 2)))

divideTest :: Statement
divideTest = Assign "Z" (Op Divide (Val (IntVal 6)) (Val (IntVal 2)))

------ Other Ops
gtTest :: Statement
gtTest = Assign "Z" (Op Gt (Val (IntVal 3)) (Val (IntVal 2)))

geTest :: Statement
geTest = Assign "Z" (Op Ge (Val (IntVal 3)) (Val (IntVal 2)))
geTest2 :: Statement
geTest2 = Assign "Z" (Op Le (Val (IntVal 3)) (Val (IntVal 3)))

ltTest :: Statement
ltTest = Assign "Z" (Op Lt (Val (IntVal 3)) (Val (IntVal 2)))

leTest :: Statement
leTest = Assign "Z" (Op Le (Val (IntVal 3)) (Val (IntVal 2)))
leTest2 :: Statement
leTest2 = Assign "Z" (Op Le (Val (IntVal 3)) (Val (IntVal 3)))
----

--Should result in Z being 5 and X being 1
sequenceTest :: Statement
sequenceTest = Sequence plusTest minusTest

-- should result in x = 1 y = 2 z = 3
sequenceTest2 :: Statement
sequenceTest2 = Sequence (Sequence vX vY) (Assign "Z" (Op Plus (Var "X") (Var "Y")))

----------------------

--Should return x is 1
ifTest :: Statement
ifTest = If (Val (BoolVal True)) vX vY

--Should return y is 2
ifTest2 :: Statement
ifTest2 = If (Val (BoolVal False)) vX vY

-----------------

whileTest :: Statement
whileTest = Sequence (vX) (While (Op Gt (Var "X") (Val (IntVal 0))) (Assign "X" (Val (IntVal 0))))

----------------
wTest :: Statement
wTest = Sequence (Assign "X" (Op Plus (Op Minus (Op Plus (Val (IntVal 1)) (Val (IntVal 2))) (Val (IntVal 3))) (Op Plus (Val (IntVal 1)) (Val (IntVal 3))))) (Sequence (Assign "Y" (Val (IntVal 0))) (While (Op Gt (Var "X") (Val (IntVal 0))) (Sequence (Assign "Y" (Op Plus (Var "Y") (Var "X"))) (Assign "X" (Op Minus (Var "X") (Val (IntVal 1)))))))

wFact :: Statement
wFact = Sequence (Assign "N" (Val (IntVal 2))) (Sequence (Assign "F" (Val (IntVal 1))) (While (Op Gt (Var "N") (Val (IntVal 0))) (Sequence (Assign "X" (Var "N")) (Sequence (Assign "Z" (Var "F")) (Sequence (While (Op Gt (Var "X") (Val (IntVal 1))) (Sequence (Assign "F" (Op Plus (Var "Z") (Var "F"))) (Assign "X" (Op Minus (Var "X") (Val (IntVal 1)))))) (Assign "N" (Op Minus (Var "N") (Val (IntVal 1)))))))))

t4a :: Test 
t4a = execS wTest Map.empty ~?= 
        Map.fromList [("X",IntVal 0),("Y",IntVal 10)]

t4b :: Test
t4b = execS wFact Map.empty ~?=
        Map.fromList [("F",IntVal 2),("N",IntVal 0),("X",IntVal 1),("Z",IntVal 2)]

